from git import Repo
from git.repo.fun import find_submodule_git_dir
from exceptions import CheckoutException, CodeShovelMethodFailedAgainException, Not200Exception
from strictyaml import load
from db import MYSQL_CONNECTION
import sys
import os
import time


with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data


def assert_checkout(repo, project):
    try:
        assert not repo.bare
        #print(repo.active_branch.name)
        #assert repo.active_branch.name == project["main-branch"]
        assert repo.head.commit.hexsha == project["commit-n-sha"]
    except:
        raise CheckoutException

def update_repo_progress_to_db(connection, name, count, stars, fork, total_files, contributors, oldest_commit, oldest_timestamp):
    with connection.cursor() as cursor:
        sql = f"SELECT id FROM csrepo WHERE name=%s"
        cursor.execute(sql, (name))
        
        if cursor.rowcount == 0:
            print(f"project {name} has no entry")
            return 
        elif cursor.rowcount > 1:
            print(f"project {name} has multiple entry. This should really not have happened!")
            return 
        
        id = cursor.fetchone()[0]

        sql = f"UPDATE csrepo SET progress = 100, commit_count = %s, github_stars = %s , github_forks = %s, "\
              f"file_count = %s, contributor_count = %s, oldest_commit = %s, oldest_commit_authored_date = %s "\
              f"WHERE id = %s"
        cursor.execute(
                sql, (
                    count, stars, fork, total_files, contributors, oldest_commit, oldest_timestamp, id
                )
            )
        connection.commit()
        return cursor.lastrowid


def count_all_files(dir_path):
    total = 0
    for root, dirs, files in os.walk(dir_path):
        total += len(files)
    return total

if __name__ == "__main__":
    # for project_key in PROJECTS:
    #     project = PROJECTS[project_key]
        project = PROJECTS['graal']

        if "progress" in project and project["progress"] == "100":
            repo = Repo(project["local-repo-dir"])
            print(f"\n**** {project['name']} ****")
            ## checkout to our specified revisions
            try:
                assert_checkout(repo, project)
            except: 
                print(f"{project['name']} git ref not as specified for analysis. \n {project['local-repo-dir']}")
                repo.git.checkout(project["commit-n-sha"], force=True)
                assert_checkout(repo, project)

            total_files = count_all_files(project["local-repo-dir"])
            
            summary_log = repo.git.shortlog(summary=True, numbered=True, email=True )
            contributors = len(summary_log.split('\n'))

            latest_commit = repo.commit('HEAD')
            # print(time.gmtime(latest_commit.authored_date))

            first_commit_str = repo.git.rev_list("HEAD", max_parents=0)
            first_commits = first_commit_str.split('\n')
            oldest_timestamp = None
            oldest_commit = None
            for first_commit in first_commits:
                #print(first_commit)
                commit = repo.commit(first_commit)
                if oldest_timestamp is None or commit.authored_date < oldest_timestamp:
                    oldest_timestamp = commit.authored_date
                    oldest_commit = commit

            print(project['name'], repo.head.commit.count(), project['github-star'], project['github-fork'], total_files, contributors, oldest_commit.hexsha, oldest_commit.authored_date)
            # update_repo_progress_to_db(MYSQL_CONNECTION, project["name"], repo.head.commit.count(), project['github-star'], project['github-fork'], total_files, contributors, oldest_commit.hexsha, time.gmtime(oldest_commit.authored_date ))
