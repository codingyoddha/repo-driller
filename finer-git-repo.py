from git import Repo
from exceptions import CheckoutException
from strictyaml import load
from pathlib import Path
import os
import subprocess

with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data


def assert_checkout(repo, project):
    try:
        assert not repo.bare
        #print(repo.active_branch.name)
        #assert repo.active_branch.name == project["main-branch"]
        assert repo.head.commit.hexsha == project["commit-n-sha"]
    except:
        raise CheckoutException

FINER_GIT_BASE_PATH = "/Users/prashamojha/prsm/finergit/"

##

sys.exit("This has finished  once. Are you sure you want to do it agian? ")

if __name__ == "__main__":
    # for project_key in PROJECTS:
    #     project = PROJECTS[project_key]
        project = PROJECTS['quarkus']

        if "progress" in project and project["progress"] == "100":
            src_path = project["local-repo-dir"]
            dst_path = project["finer-repo-dir"]

            dst_full_path = os.path.join(FINER_GIT_BASE_PATH, dst_path)
            print(dst_full_path)

            try:
                Path(dst_full_path).mkdir(parents=True, exist_ok=False)
            except FileExistsError as err:
                print("Directory already exists. Skipping..")
                # continue

            repo = Repo(src_path)
            print(f"\n**** {project['name']} ****")
            ## checkout to our specified revisions
            try:
                assert_checkout(repo, project)
            except:
                print(f"{project['name']} git ref not as specified for analysis. \n {project['local-repo-dir']}")
                repo.git.checkout(project["commit-n-sha"], force=True)
                assert_checkout(repo, project)

            # create --src ~/prsm/codeshovel/cache/github.com/google/guava --des ~/prsm/thesis/fgit/guava5
            subprocess.call(['java', '-jar', 'FinerGit-all.jar', 'create', '--src', src_path, '--des', dst_full_path])

