gcp:
	gcloud compute instances list --format='json' | jq '.[] | {(.name): .networkInterfaces[0].accessConfigs[0].natIP}' | jq -s '.' > ip.json