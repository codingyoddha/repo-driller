from strictyaml import load
from git import Repo
from db import MYSQL_CONNECTION
from pydriller import Repository, ModificationType
from exceptions import CheckoutException
import sys
import traceback
import os
import json
from javaparser import Parser

FROM_EXCLUSIVE_ID = 0
TO_INCLUSIVE_ID = 100

with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data

def assert_checkout(repo, project):
    try:
        assert not repo.bare
        assert repo.head.commit.hexsha == project["commit-n-sha"]
    except:
        raise CheckoutException


def transform_change_type(change_type):
    if change_type == ModificationType.ADD:
        return "add"
    elif change_type == ModificationType.COPY:
        return "copy"
    elif change_type == ModificationType.RENAME:
        return "rename"
    elif change_type == ModificationType.DELETE:
        return "delete"
    elif change_type == ModificationType.MODIFY:
        return "modify"
    else:
        return "unknown"


# @returns return id, already_exists
def get_repo_from_db(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"SELECT * FROM csrepo WHERE name=%s"
        cursor.execute(sql, (kwargs.get("name")))
        if cursor.rowcount == 1:
            return cursor.fetchone()

        raise Exception


def get_commit_id_from_db(connection, repo_id, sha):
    if sha is None:
        return None

    if repo_id is None:
        raise

    with connection.cursor() as cursor:
        sql = f"SELECT id FROM cscommit WHERE repo_id = %s and sha = %s"
        cursor.execute(sql, (repo_id, sha))
        if cursor.rowcount == 1:
            return cursor.fetchone()[0]
    
    return None


def insert_java_metrics(connection, repo_id, commit_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:

            sql = f"INSERT INTO java_file_metrics (repo_id, commit_id, total_files, added, modified, renamed, copied, deleted, other," \
                  f" total_added_lines, total_deleted_lines, total_nloc, total_methods_after, total_methods_before, total_changed_methods ) " \
                  f" VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(
                sql, (
                    repo_id,
                    commit_id,
                    kwargs.get("total_files", None),
                    kwargs.get("added", None),
                    kwargs.get("modified", None),
                    kwargs.get("renamed", None),
                    kwargs.get("copied", None),
                    kwargs.get("deleted", None),
                    kwargs.get("other", None),
                    kwargs.get("total_added_lines", None),
                    kwargs.get("total_deleted_lines", None),
                    kwargs.get("total_nloc", None),
                    kwargs.get("total_methods_after", None),
                    kwargs.get("total_methods_before", None),
                    kwargs.get("total_changed_methods", None),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except Exception as err:
        print("error while insert_file_to_db: ", repo_id, kwargs)
        traceback.print_exc()
        raise err

completed_repo_ids = [
    1,2,3,4,5,6,7,9,10,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,36,37,38,40,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,64,68,69,70
]

 
# failed_repo_ids = [35, 39, 41, 62]
failed_repo_ids = [66]


if __name__ == "__main__":
    print("Repo Driller")

    for project_key in PROJECTS:
        project = PROJECTS[project_key]
        # project = PROJECTS['capacitor']


        if "progress" in project and project["progress"] == "100":
            repo_path = project["local-repo-dir"]
            repo = Repo(repo_path)


            print(f"\n**** {project['name']} ****")
            ## checkout to our specified revisions
            try:
                assert_checkout(repo, project)
            except: 
                print(f"{project['name']} git ref not as specified for analysis. \n {project['local-repo-dir']}")
                repo.git.checkout(project["commit-n-sha"], force=True)
                assert_checkout(repo, project)

            db_repo = get_repo_from_db(MYSQL_CONNECTION, **project)
            print(f"repo_id {db_repo}")

            repo_id = db_repo[0]

            if repo_id is None:
                sys.exit("repo_id cannot be null!")

            if not(repo_id > FROM_EXCLUSIVE_ID and repo_id <= TO_INCLUSIVE_ID):
                print("..not within the range processing ..")
                # continue

            if repo_id in completed_repo_ids or repo_id in failed_repo_ids:
                print("skipping already ......")
                # .continue
            
            for idx, val in enumerate(db_repo):
                print(idx, val)
            
            for commit in Repository(path_to_repo=repo_path, only_modifications_with_file_types=['.java'], order="reverse").traverse_commits():
                #print(f"inserting commit {commit.hash}")

                commit_id = get_commit_id_from_db(MYSQL_CONNECTION, repo_id, commit.hash)

                total_java_files_touched = 0
                added_java_files = 0
                modified_java_files = 0
                renamed_java_files = 0
                copied_java_files = 0
                deleted_java_files = 0
                other_java_files = 0

                total_added_lines = 0
                total_deleted_lines = 0
                total_nloc = 0
                total_methods_after = 0
                total_methods_before = 0
                total_changed_methods = 0


                for modification in commit.modified_files:
                    if isinstance(modification.filename, str) and modification.filename.endswith(".java"):
                        total_java_files_touched += 1

                        change_type = transform_change_type(modification.change_type)
                        if change_type == "add":
                            added_java_files += 1
                        elif change_type == "rename":
                            renamed_java_files += 1
                        elif change_type == "modify":
                            modified_java_files += 1
                        elif change_type == "copy":
                            copied_java_files += 1
                        elif change_type == "delete":
                            deleted_java_files += 1
                        else:
                            other_java_files += 1

                        total_added_lines = 0
                        total_deleted_lines = 0
                        total_nloc = 0
                        total_methods_after = 0
                        total_methods_before = 0
                        total_changed_methods = 0

                        if modification.added_lines is not None:
                            total_added_lines += modification.added_lines

                        if modification.deleted_lines is not None:
                            total_deleted_lines += modification.deleted_lines

                        if modification.nloc is not None:
                            total_nloc += modification.nloc

                        if modification.methods is not None:
                            no_of_methods_after = len(modification.methods)
                            if no_of_methods_after is not None:
                                total_methods_after += no_of_methods_after

                        if modification.methods_before is not None:
                            no_of_methods_before = len(modification.methods_before)
                            if no_of_methods_before is not None:
                                total_methods_before += no_of_methods_before

                        if modification.changed_methods is not None:
                            no_of_methods_changed = len(modification.changed_methods)
                            if no_of_methods_changed is not None:
                                total_changed_methods += no_of_methods_changed



                insert_java_metrics(MYSQL_CONNECTION,
                                    repo_id=repo_id,
                                    commit_id=commit_id,
                                    total_files=total_java_files_touched,
                                    added=added_java_files,
                                    modified=modified_java_files,
                                    renamed=renamed_java_files,
                                    copied=copied_java_files,
                                    deleted=deleted_java_files,
                                    other=other_java_files,
                                    total_added_lines=total_added_lines,
                                    total_deleted_lines=total_deleted_lines,
                                    total_nloc=total_nloc,
                                    total_methods_after=total_methods_after,
                                    total_methods_before=total_methods_before,
                                    total_changed_methods=total_changed_methods
                                    )


            print(f"** done ** {project['name']} ")