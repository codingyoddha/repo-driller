from pydriller import Repository, ModificationType
import pymysql.cursors
import os
import json

BASE_REPO_PATH = "../repos/"


def get_repos():
    # build up the list of repos
    repos = [
        "jmeter"
    ]

    return [os.path.join(BASE_REPO_PATH, repo) for repo in repos]
    #return ["./"]


def insert_repo(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO repo (name, url) VALUES (%s, %s)"
        cursor.execute(
            sql,
            (kwargs.get("name"), kwargs.get("url", ""))
            )
        #connection.commit()
        return cursor.lastrowid


def insert_file(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO file (commit_id, name, extension, change_type, deleted_lines, added_lines, old_path, new_path, source_code, source_code_before, token_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(
            sql, (
                kwargs.get("commit_id"),
                kwargs.get("name", ""),
                kwargs.get("extension", ""),
                kwargs.get("change_type", ""),
                kwargs.get("deleted_lines", ""),
                kwargs.get("added_lines", ""),
                kwargs.get("old_path", ""),
                kwargs.get("new_path", ""),
                kwargs.get("source_code", ""),
                kwargs.get("source_code_before", ""),
                kwargs.get("token_count", ""),
            )
        )
        #connection.commit()
        return cursor.lastrowid


def insert_commit(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO commit (repo_id, hash, author, email, timestamp, message, deletions, files, insertions, no_lines, merge ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        cursor.execute(
            sql, (
                kwargs.get("repo_id"),
                kwargs.get("hash"),
                kwargs.get("author", ""),
                kwargs.get("email", ""),
                kwargs.get("timestamp", ""),
                kwargs.get("message", ""),
                kwargs.get("deletions", ""),
                kwargs.get("files", ""),
                kwargs.get("insertions", ""),
                kwargs.get("no_lines", ""),
                kwargs.get("merge", ""),
            )
        )
        #connection.commit()
        return cursor.lastrowid


def insert_commit_parents(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO commit_relation (commit_hash, commit_id, parent_hash) VALUES (%s, %s, %s )"

        cursor.execute(
            sql, (
                kwargs.get("commit_hash"),
                kwargs.get("commit_id", ""),
                kwargs.get("parent_hash")
            )
        )
        #connection.commit()
        return cursor.lastrowid


def insert_method(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO method (file_id, name, filename, long_name, length, nloc, end_line, " \
              f"start_line, token_count, top_nesting_level, unit_complexity_low_risk_threshold, " \
              f"unit_interfacing_low_risk_threshold, unit_size_low_risk_threshold, parameters, " \
              f"fan_in, fan_out, general_fan_out, complexity) " \
              f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        cursor.execute(
            sql, (
                kwargs.get("file_id"),
                kwargs.get("name"),
                kwargs.get("filename"),
                kwargs.get("long_name"),
                kwargs.get("length"),
                kwargs.get("nloc"),
                kwargs.get("end_line"),
                kwargs.get("start_line"),
                kwargs.get("token_count"),
                kwargs.get("top_nesting_level"),
                kwargs.get("unit_complexity_low_risk_threshold"),
                kwargs.get("unit_interfacing_low_risk_threshold"),
                kwargs.get("unit_size_low_risk_threshold"),
                json.dumps(kwargs.get("parameters")),
                kwargs.get("fan_in"),
                kwargs.get("fan_out"),
                kwargs.get("general_fan_out"),
                kwargs.get("complexity")
            )
        )
        #connection.commit()
        return cursor.lastrowid


def transform_change_type(change_type):
    if change_type == ModificationType.ADD:
        return "add"
    elif change_type == ModificationType.COPY:
        return "copy"
    elif change_type == ModificationType.RENAME:
        return "rename"
    elif change_type == ModificationType.DELETE:
        return "delete"
    elif change_type == ModificationType.MODIFY:
        return "modify"
    else:
        return "unknown"


def main():
    connection = pymysql.connect(
        host='localhost',
        user='root',
        password='root',
        database='git'
    )

    for repo in get_repos():
        last_inserted_repo_id = insert_repo(connection, name=repo)
        print("repo id: ", last_inserted_repo_id)

        for commit in Repository(path_to_repo=get_repos()).traverse_commits():
            print(f"inserting commit {commit.hash}")
            last_inserted_commit_id =  insert_commit(
                connection,
                repo_id=last_inserted_repo_id,
                hash=commit.hash,
                author=commit.author.name,
                email=commit.author.email,
                timestamp=commit.author_date,
                message=commit.msg,
                deletions=commit.deletions,
                files=commit.files,
                insertions=commit.insertions,
                no_lines=commit.lines,
                merge=commit.merge


            )
            print("commit id: ", last_inserted_commit_id)

            for parent_hash in commit.parents:
                insert_commit_parents(connection,
                                      commit_hash=commit.hash,
                                      commit_id=last_inserted_commit_id,
                                      parent_hash=parent_hash)

            for modification in commit.modified_files:
                filepath = modification.filename.split(".")
                extension = ""
                if len(filepath) > 1:
                    extension = filepath[-1]

                last_inserted_file_id = insert_file(
                    connection,
                    commit_id=last_inserted_commit_id,
                    name=modification.filename,
                    extension=extension,
                    change_type=transform_change_type(modification.change_type),
                    deleted_lines=modification.deleted_lines,
                    added_lines=modification.added_lines,
                    old_path=modification.old_path,
                    new_path=modification.new_path,
                    source_code=modification.source_code,
                    source_code_before=modification.source_code_before,
                    token_count=modification.token_count

                )

                for method in modification.methods:
                    insert_method(connection,
                                  file_id=last_inserted_file_id,
                                  name=method.name,
                                  filename=method.filename,
                                  long_name=method.long_name,
                                  length=method.length,
                                  nloc=method.nloc,
                                  end_line=method.end_line,
                                  start_line=method.start_line,
                                  token_count=method.token_count,
                                  top_nesting_level=method.top_nesting_level,
                                  unit_complexity_low_risk_threshold=method.UNIT_COMPLEXITY_LOW_RISK_THRESHOLD,
                                  unit_interfacing_low_risk_threshold=method.UNIT_INTERFACING_LOW_RISK_THRESHOLD,
                                  unit_size_low_risk_threshold=method.UNIT_SIZE_LOW_RISK_THRESHOLD,
                                  parameters=method.parameters,
                                  fan_in=method.fan_in,
                                  fan_out=method.fan_out,
                                  general_fan_out=method.general_fan_out,
                                  complexity=method.complexity)


if __name__ == "__main__":
    main()
