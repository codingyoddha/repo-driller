from git.objects import commit
from strictyaml import load
from git import Repo
from db import MYSQL_CONNECTION
from pydriller import Repository, ModificationType
from exceptions import CheckoutException
import sys
from pydriller.metrics.process.commits_count import CommitsCount
from pydriller.metrics.process.code_churn import CodeChurn
from pydriller.metrics.process.lines_count import LinesCount
from pydriller.metrics.process.hunks_count import HunksCount
from pydriller.metrics.process.contributors_count import ContributorsCount
from pydriller.metrics.process.contributors_experience import ContributorsExperience
import traceback

import os


FROM_EXCLUSIVE_ID = 100
TO_INCLUSIVE_ID = 100

with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data

def assert_checkout(repo, project):
    try:
        assert not repo.bare
        assert repo.head.commit.hexsha == project["commit-n-sha"]
    except:
        raise CheckoutException

# @returns return id, already_exists
def get_repo_from_db(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"SELECT * FROM csrepo WHERE name=%s"
        cursor.execute(sql, (kwargs.get("name")))
        if cursor.rowcount == 1:
            return cursor.fetchone()

        raise Exception


def get_file_id_from_db(connection, repo_id, file_path):
    if file_path is None:
        return None

    if repo_id is None:
        raise

    with connection.cursor() as cursor:
        sql = f"SELECT id FROM csfile WHERE repo_id = %s and file_path = %s"
        cursor.execute(sql, (repo_id, file_path))
        if cursor.rowcount > 0:
            return cursor.fetchone()[0]
    
    return None

def insert_file_commit_to_db(connection, repo_id, file_id, file_name, file_path, **kwargs):
    if repo_id is None:
        raise

    # print("kwargs => ", kwargs)
    # return

    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO csfilecommit(repo_id, file_id, file_name, file_path, "\
                  f"churn_count, churn_max, churn_avg, contributor_count, contributor_count_minor, "\
                  f"contributor_exp_count, commit_count, hunks_count, lines_count, " \
                  f" lines_count_added, lines_max_added, lines_avg_added, lines_count_removed, lines_max_removed, lines_avg_removed ) " \
                  f"VALUES (%s, %s, %s, %s, "\
                  f"%s, %s, %s, %s, %s, %s, %s, %s, "\
                  f"%s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(
                sql, (
                    repo_id,
                    file_id, 
                    file_name,
                    file_path,
                    kwargs.get("churn_count", None),
                    kwargs.get("churn_max", None),
                    kwargs.get("churn_avg", None),
                    kwargs.get("contributor_count", None),
                    kwargs.get("contributor_count_minor", None),
                    kwargs.get("contributor_exp_count", None),
                    kwargs.get("commit_count", None),
                    kwargs.get("hunks_count", None),
                    kwargs.get("lines_count", None),
                    kwargs.get("lines_count_added", None),
                    kwargs.get("lines_max_added", None),
                    kwargs.get("lines_avg_added", None),
                    kwargs.get("lines_count_removed", None),
                    kwargs.get("lines_max_removed", None),
                    kwargs.get("lines_avg_removed", None),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        traceback.print_exc()
    
        print("error while insert_file_commit_to_db: ", repo_id, file_path)
        raise



if __name__ == "__main__":
    # print("Repo Driller")

    for project_key in PROJECTS:
        project = PROJECTS[project_key]
        # project = PROJECTS['capacitor']


        if "progress" in project and project["progress"] == "100":
            repo_path = project["local-repo-dir"]
            repo = Repo(repo_path)


            print(f"\n**** {project['name']} ****")
            ## checkout to our specified revisions
            try:
                assert_checkout(repo, project)
            except: 
                print(f"{project['name']} git ref not as specified for analysis. \n {project['local-repo-dir']}")
                repo.git.checkout(project["commit-n-sha"], force=True)
                assert_checkout(repo, project)

            db_repo = get_repo_from_db(MYSQL_CONNECTION, **project)
            print(f"repo_id {db_repo}")

            repo_id = db_repo[0]

            if repo_id is None:
                sys.exit("repo_id cannot be null!")

            if not(repo_id > FROM_EXCLUSIVE_ID and repo_id <= TO_INCLUSIVE_ID):
                print("..no within the range processing ..")
                continue
            for idx, val in enumerate(db_repo):
                print(idx, val)

            commit_metric = CommitsCount(path_to_repo=repo_path, 
            from_commit=db_repo[14],
            to_commit=db_repo[6])

            commit_count = commit_metric.count()

            churn_metric = CodeChurn(path_to_repo=repo_path, 
            from_commit=db_repo[14],
            to_commit=db_repo[6])

            churn_count = churn_metric.count()
            churn_max = churn_metric.max()
            churn_avg = churn_metric.avg()

            contributors_metric = ContributorsCount(path_to_repo=repo_path, 
            from_commit=db_repo[14],
            to_commit=db_repo[6])

            contrib_count = contributors_metric.count()
            contrib_minor = contributors_metric.count_minor()

            contributors_exp_metric = ContributorsExperience(path_to_repo=repo_path, 
            from_commit=db_repo[14],
            to_commit=db_repo[6])

            contrib_exp_count = contributors_exp_metric.count()

            hunk_metric = HunksCount(path_to_repo=repo_path, 
            from_commit=db_repo[14],
            to_commit=db_repo[6])

            hunk_count = hunk_metric.count()

            lines_metric = LinesCount(path_to_repo=repo_path, 
            from_commit=db_repo[14],
            to_commit=db_repo[6])

            lines_count = lines_metric.count()
            lines_count_added = lines_metric.count_added()
            lines_max_added = lines_metric.max_added()
            lines_avg_added = lines_metric.avg_added()
            lines_count_removed = lines_metric.count_removed()
            lines_max_removed = lines_metric.max_removed()
            lines_avg_removed = lines_metric.avg_removed()

            for file_path in commit_count:
                if file_path is None or not isinstance(file_path, str):
                    print("skipping.. ", file_path)
                    continue

                try:
                    churn_count_val=churn_count[file_path] if file_path in churn_count else None
                    churn_max_val=churn_max[file_path] if file_path in churn_max  else None
                    churn_avg_val=churn_avg[file_path] if file_path in churn_avg else None
                    contributor_count_val=contrib_count[file_path] if file_path in contrib_count else None
                    contributor_count_minor_val=contrib_minor[file_path] if file_path in contrib_minor else None
                    contributor_exp_count_val=contrib_exp_count[file_path] if file_path in contrib_exp_count else None
                    commit_count_val=commit_count[file_path] if file_path in commit_count else None
                    hunks_count_val=hunk_count[file_path] if file_path in hunk_count  else None
                    lines_count_val=lines_count[file_path] if file_path in lines_count else None
                    lines_count_added_val=lines_count_added[file_path] if file_path in lines_count_added else None
                    lines_max_added_val=lines_max_added[file_path] if file_path in lines_max_added else None
                    lines_avg_added_val=lines_avg_added[file_path] if file_path in lines_avg_added else None
                    lines_count_removed_val=lines_count_removed[file_path] if file_path in  lines_count_removed else None
                    lines_max_removed_val=lines_max_removed[file_path] if file_path in lines_max_removed else None
                    lines_avg_removed_val=lines_avg_removed[file_path] if file_path in lines_avg_removed else None


                    if file_path.endswith(".java"):
                        file_id = get_file_id_from_db(MYSQL_CONNECTION,repo_id, file_path)
                        file_name = os.path.basename(file_path)
                        insert_file_commit_to_db(MYSQL_CONNECTION, 
                            repo_id, 
                            file_id, 
                            file_name, 
                            file_path, 
                            churn_count=churn_count_val,
                            churn_max=churn_max_val,
                            churn_avg=churn_avg_val,
                            contributor_count=contributor_count_val,
                            contributor_count_minor=contributor_count_minor_val,
                            contributor_exp_count=contributor_exp_count_val,
                            commit_count=commit_count_val,
                            hunks_count=hunks_count_val,
                            lines_count=lines_count_val,
                            lines_count_added=lines_count_added_val,
                            lines_max_added=lines_max_added_val,
                            lines_avg_added=lines_avg_added_val,
                            lines_count_removed=lines_count_removed_val,
                            lines_max_removed=lines_max_removed_val,
                            lines_avg_removed=lines_avg_removed_val,
                    
                        )
                except:
                    traceback.print_exc()
                    sys.exit()
                    print("error => ", file_path)



            print(f"** done ** {project['name']} ")