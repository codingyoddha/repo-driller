from strictyaml import load
from git import Repo
from db import MYSQL_CONNECTION
from pydriller import Repository, ModificationType
from exceptions import CheckoutException
import sys
import traceback
import os
import json
from javaparser import Parser

FROM_EXCLUSIVE_ID = 43
TO_INCLUSIVE_ID = 43

with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data

def assert_checkout(repo, project):
    try:
        assert not repo.bare
        assert repo.head.commit.hexsha == project["commit-n-sha"]
    except:
        raise CheckoutException


def transform_change_type(change_type):
    if change_type == ModificationType.ADD:
        return "add"
    elif change_type == ModificationType.COPY:
        return "copy"
    elif change_type == ModificationType.RENAME:
        return "rename"
    elif change_type == ModificationType.DELETE:
        return "delete"
    elif change_type == ModificationType.MODIFY:
        return "modify"
    else:
        return "unknown"


# @returns return id, already_exists
def get_repo_from_db(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"SELECT * FROM csrepo WHERE name=%s"
        cursor.execute(sql, (kwargs.get("name")))
        if cursor.rowcount == 1:
            return cursor.fetchone()

        raise Exception


def get_file_id_from_db(connection, repo_id, file_path):
    if file_path is None:
        return None

    if repo_id is None:
        raise

    with connection.cursor() as cursor:
        sql = f"SELECT id FROM csfile WHERE repo_id = %s and file_path = %s"
        cursor.execute(sql, (repo_id, file_path))
        if cursor.rowcount > 0:
            return cursor.fetchone()[0]
    
    return None

def get_commit_id_from_db(connection, repo_id, sha):
    if sha is None:
        return None

    if repo_id is None:
        raise

    with connection.cursor() as cursor:
        sql = f"SELECT id FROM cscommit WHERE repo_id = %s and sha = %s"
        cursor.execute(sql, (repo_id, sha))
        if cursor.rowcount == 1:
            return cursor.fetchone()[0]
    
    return None

def insert_commit_to_db(connection, repo_id, commit_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO pycommit (repo_id, cscommit_id, sha, message, author, authored_date, author_timezone, author_email, commiter, "\
                  f"commiter_email, commited_date, commiter_timezone, merge, files, no_lines, insertions, deletions, modified, dmm_unit_complexity, dmm_unit_interfacing, dmm_unit_size  ) " \
                  f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

            cursor.execute(
                sql, (
                    repo_id,
                    commit_id,
                    kwargs.get("sha"),
                    kwargs.get("message", ""),
                    kwargs.get("author", ""),
                    kwargs.get("authored_date", ""),
                    kwargs.get("author_timezone", ""),
                    kwargs.get("author_email", ""),
                    kwargs.get("commiter", ""),
                    kwargs.get("commiter_email", ""),
                    kwargs.get("commited_date", ""),
                    kwargs.get("commiter_timezone", ""),
                    kwargs.get("merge", ""),
                    kwargs.get("files", ""),
                    kwargs.get("no_lines", ""),
                    kwargs.get("insertions", ""),
                    kwargs.get("deletions", ""),
                    kwargs.get("modified", ""),
                    kwargs.get("dmm_unit_complexity", ""),
                    kwargs.get("dmm_unit_interfacing", ""),
                    kwargs.get("dmm_unit_size", ""),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except Exception as err:
        print("error while insert_commit_to_db: ", repo_id, commit_id, kwargs)
        traceback.print_exc()
        raise err


def insert_file(connection, repo_id, commit_id, file_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            # sql = f"INSERT INTO pyfile (repo_id, commit_id, file_id, name, extension, change_type, deleted_lines, " \
            #       f"added_lines, old_path, new_path, source_code, source_code_before, token_count, nloc, diff) " \
            #       f" VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            sql = f"INSERT INTO pyfile (repo_id, commit_id, file_id, name, extension, change_type, deleted_lines, "\
                  f"added_lines, old_path, new_path, token_count, nloc ) "\
                  f" VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(
                sql, (
                    repo_id,
                    commit_id,
                    file_id,
                    kwargs.get("name", ""),
                    kwargs.get("extension", ""),
                    kwargs.get("change_type", ""),
                    kwargs.get("deleted_lines", ""),
                    kwargs.get("added_lines", ""),
                    kwargs.get("old_path", ""),
                    kwargs.get("new_path", ""),

                    kwargs.get("token_count", ""),
                    kwargs.get("nloc", ""),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except Exception as err:
        print("error while insert_file_to_db: ", repo_id, kwargs)
        traceback.print_exc()
        raise err


def insert_method(connection, file_id, repo_id, commit_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO pymethod (file_id, repo_id, commit_id, name, filename, long_name, length, nloc, end_line, " \
              f"start_line, token_count, top_nesting_level, unit_complexity_low_risk_threshold, " \
              f"unit_interfacing_low_risk_threshold, unit_size_low_risk_threshold, parameters, " \
              f"fan_in, fan_out, general_fan_out, complexity) " \
              f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

            cursor.execute(
                sql, (
                    file_id, 
                    repo_id, 
                    commit_id,
                    kwargs.get("name"),
                    kwargs.get("filename"),
                    kwargs.get("long_name"),
                    kwargs.get("length"),
                    kwargs.get("nloc"),
                    kwargs.get("end_line"),
                    kwargs.get("start_line"),
                    kwargs.get("token_count"),
                    kwargs.get("top_nesting_level"),
                    kwargs.get("unit_complexity_low_risk_threshold"),
                    kwargs.get("unit_interfacing_low_risk_threshold"),
                    kwargs.get("unit_size_low_risk_threshold"),
                    json.dumps(kwargs.get("parameters")),
                    kwargs.get("fan_in"),
                    kwargs.get("fan_out"),
                    kwargs.get("general_fan_out"),
                    kwargs.get("complexity")
                )
            )
            #connection.commit()
            return cursor.lastrowid
    except Exception as err:
        print("error while insert_method_to_db: ", repo_id,  file_id, kwargs)
        traceback.print_exc()
        raise err

def insert_before_method(connection, file_id, repo_id, commit_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO pybeforemethod (file_id, repo_id, commit_id, name, filename, long_name, length, nloc, end_line, " \
              f"start_line, token_count, top_nesting_level, unit_complexity_low_risk_threshold, " \
              f"unit_interfacing_low_risk_threshold, unit_size_low_risk_threshold, parameters, " \
              f"fan_in, fan_out, general_fan_out, complexity) " \
              f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

            cursor.execute(
                sql, (
                    file_id, 
                    repo_id, 
                    commit_id,
                    kwargs.get("name"),
                    kwargs.get("filename"),
                    kwargs.get("long_name"),
                    kwargs.get("length"),
                    kwargs.get("nloc"),
                    kwargs.get("end_line"),
                    kwargs.get("start_line"),
                    kwargs.get("token_count"),
                    kwargs.get("top_nesting_level"),
                    kwargs.get("unit_complexity_low_risk_threshold"),
                    kwargs.get("unit_interfacing_low_risk_threshold"),
                    kwargs.get("unit_size_low_risk_threshold"),
                    json.dumps(kwargs.get("parameters")),
                    kwargs.get("fan_in"),
                    kwargs.get("fan_out"),
                    kwargs.get("general_fan_out"),
                    kwargs.get("complexity")
                )
            )
            #connection.commit()
            return cursor.lastrowid
    except Exception as err:
        print("error while insert_before_method_to_db: ", repo_id,  file_id, kwargs)
        traceback.print_exc()
        raise err


def insert_changed_method(connection, file_id, repo_id, commit_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO pychangedmethod (file_id, repo_id, commit_id, name, filename, long_name, length, nloc, end_line, " \
              f"start_line, token_count, top_nesting_level, unit_complexity_low_risk_threshold, " \
              f"unit_interfacing_low_risk_threshold, unit_size_low_risk_threshold, parameters, " \
              f"fan_in, fan_out, general_fan_out, complexity) " \
              f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

            cursor.execute(
                sql, (
                    file_id, 
                    repo_id, 
                    commit_id,
                    kwargs.get("name"),
                    kwargs.get("filename"),
                    kwargs.get("long_name"),
                    kwargs.get("length"),
                    kwargs.get("nloc"),
                    kwargs.get("end_line"),
                    kwargs.get("start_line"),
                    kwargs.get("token_count"),
                    kwargs.get("top_nesting_level"),
                    kwargs.get("unit_complexity_low_risk_threshold"),
                    kwargs.get("unit_interfacing_low_risk_threshold"),
                    kwargs.get("unit_size_low_risk_threshold"),
                    json.dumps(kwargs.get("parameters")),
                    kwargs.get("fan_in"),
                    kwargs.get("fan_out"),
                    kwargs.get("general_fan_out"),
                    kwargs.get("complexity")
                )
            )
            #connection.commit()
            return cursor.lastrowid
    except Exception as err:
        print("error while insert_method_to_db: ", repo_id,  file_id, kwargs)
        traceback.print_exc()
        raise err



completed_repo_ids = [
]

 
failed_repo_ids = [35, 39, 41, 62]

if __name__ == "__main__":
    # print("Repo Driller")

    #for project_key in PROJECTS:
        #project = PROJECTS[project_key]
        project = PROJECTS['capacitor']


        if "progress" in project and project["progress"] == "100":
            repo_path = project["local-repo-dir"]
            repo = Repo(repo_path)


            print(f"\n**** {project['name']} ****")
            ## checkout to our specified revisions
            try:
                assert_checkout(repo, project)
            except: 
                print(f"{project['name']} git ref not as specified for analysis. \n {project['local-repo-dir']}")
                repo.git.checkout(project["commit-n-sha"], force=True)
                assert_checkout(repo, project)

            db_repo = get_repo_from_db(MYSQL_CONNECTION, **project)
            print(f"repo_id {db_repo}")

            repo_id = db_repo[0]

            if repo_id is None:
                sys.exit("repo_id cannot be null!")

            if not(repo_id > FROM_EXCLUSIVE_ID and repo_id <= TO_INCLUSIVE_ID):
                print("..not within the range processing ..")
                # continue

            if repo_id in completed_repo_ids or repo_id in failed_repo_ids:
                print("skipping already ......")
                # .continue
            
            for idx, val in enumerate(db_repo):
                print(idx, val)
            
            for commit in Repository(path_to_repo=repo_path, order="reverse").traverse_commits():
                print(f"inserting commit {commit.hash}")

                commit_id = get_commit_id_from_db(MYSQL_CONNECTION, repo_id, commit.hash)
                
                last_inserted_commit_id = insert_commit_to_db(
                    MYSQL_CONNECTION,
                    repo_id,
                    commit_id,
                    sha=commit.hash,
                    message=commit.msg,
                    author=commit.author.name,
                    authored_date=commit.author_date,
                    author_timezone=commit.author_timezone,
                    author_email=commit.author.email,
                    commiter=commit.committer.name,
                    commiter_email=commit.committer.email,
                    commited_date=commit.committer_date,
                    commiter_timezone=commit.committer_timezone,
                    merge=commit.merge,
                    files=commit.files,
                    no_lines=commit.lines,
                    insertions=commit.insertions,
                    deletions=commit.deletions,
                    modified=commit.lines,
                    dmm_unit_complexity=commit.dmm_unit_complexity,
                    dmm_unit_interfacing=commit.dmm_unit_interfacing,
                    dmm_unit_size=commit.dmm_unit_size
                )

                for modification in commit.modified_files:
                    if isinstance(modification.filename, str) and modification.filename.endswith(".java"):
                        change_type = transform_change_type(modification.change_type)
                        if change_type == "add" or change_type == "rename" or change_type == "modify" or change_type == "copy":
                            file_id = get_file_id_from_db(MYSQL_CONNECTION,repo_id, modification.new_path)
                        elif change_type == "delete":
                            file_id = get_file_id_from_db(MYSQL_CONNECTION,repo_id, modification.old_path)
                        else:
                            file_id = None


                        filepath = modification.filename.split(".")
                        extension = ""
                        if len(filepath) > 1:
                            extension = filepath[-1]

                        
                        # if modification.source_code_before is not None or modification.source_code is not None:
                        #     parser = Parser(Parser.parse(modification.source_code), Parser.parse(modification.source_code_before))
                        # else:
                        #     print("source_code_before or source-code is none.")


                        last_inserted_file_id = insert_file(
                            MYSQL_CONNECTION,
                            repo_id=repo_id,
                            commit_id=last_inserted_commit_id,
                            file_id=file_id,
                            name=modification.filename,
                            extension=extension,
                            change_type=change_type,
                            deleted_lines=modification.deleted_lines,
                            added_lines=modification.added_lines,
                            old_path=modification.old_path,
                            new_path=modification.new_path,
                            source_code=modification.source_code,
                            source_code_before=modification.source_code_before,
                            token_count=modification.token_count,
                            nloc=modification.nloc,
                            diff=modification.diff
                        )
                        

                        # for method in modification.changed_methods:
                            # insert_changed_method(MYSQL_CONNECTION,
                            #         file_id=last_inserted_file_id,
                            #         repo_id=repo_id,
                            #         commit_id=last_inserted_commit_id,
                            #         name=method.name,
                            #         filename=method.filename,
                            #         long_name=method.long_name,
                            #         length=method.length,
                            #         nloc=method.nloc,
                            #         end_line=method.end_line,
                            #         start_line=method.start_line,
                            #         token_count=method.token_count,
                            #         top_nesting_level=method.top_nesting_level,
                            #         unit_complexity_low_risk_threshold=method.UNIT_COMPLEXITY_LOW_RISK_THRESHOLD,
                            #         unit_interfacing_low_risk_threshold=method.UNIT_INTERFACING_LOW_RISK_THRESHOLD,
                            #         unit_size_low_risk_threshold=method.UNIT_SIZE_LOW_RISK_THRESHOLD,
                            #         parameters=method.parameters,
                            #         fan_in=method.fan_in,
                            #         fan_out=method.fan_out,
                            #         general_fan_out=method.general_fan_out,
                            #         complexity=method.complexity)



            print(f"** done ** {project['name']} ")