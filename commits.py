from strictyaml import load
from git import Repo
from db import MYSQL_CONNECTION
from pydriller import Repository, ModificationType
from exceptions import CheckoutException
import sys
from pymysql import err as mysqlerr


with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data

def assert_checkout(repo, project):
    try:
        assert not repo.bare
        assert repo.head.commit.hexsha == project["commit-n-sha"]
    except:
        raise CheckoutException

# @returns return id, already_exists
def get_repo_from_db(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"SELECT id FROM csrepo WHERE name=%s"
        cursor.execute(sql, (kwargs.get("name")))
        if cursor.rowcount > 0:
            return cursor.fetchone()[0]

        raise Exception

def insert_commit_to_db(connection, repo_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO cscommit (repo_id, sha, message, author, authored_date, author_timezone, author_email, commiter, "\
                  f"commiter_email, commited_date, commiter_timezone, merge, files, no_lines, insertions, deletions, modified  ) " \
                  f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

            cursor.execute(
                sql, (
                    repo_id,
                    kwargs.get("sha"),
                    kwargs.get("message", ""),
                    kwargs.get("author", ""),
                    kwargs.get("authored_date", ""),
                    kwargs.get("author_timezone", ""),
                    kwargs.get("author_email", ""),
                    kwargs.get("commiter", ""),
                    kwargs.get("commiter_email", ""),
                    kwargs.get("commited_date", ""),
                    kwargs.get("commiter_timezone", ""),
                    kwargs.get("merge", ""),
                    kwargs.get("files", ""),
                    kwargs.get("no_lines", ""),
                    kwargs.get("insertions", ""),
                    kwargs.get("deletions", ""),
                    kwargs.get("modified", ""),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except mysqlerr.OperationalError as err:
        print("Oeprational error", err, kwargs.get("sha"))
        new_args = kwargs.copy()
        new_args['authored_date'] = None
        new_args['commited_date'] = None
        id = insert_commit_to_db(connection, repo_id, **new_args)
        print("returning id")
        return id
    except:
        print("error while insert_commit_to_db: ", repo_id, kwargs)
        raise


def insert_commit_parent_to_db(connection, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO cscommitparent (commit_sha, parent_sha, commit_id) "\
                  f"VALUES (%s, %s, %s)"

            cursor.execute(
                sql, (
                    kwargs.get("commit_sha"),
                    kwargs.get("parent_sha"),
                    kwargs.get("commit_id"),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while insert_commit_parent_to_db: ", kwargs)
        raise


completed_repo_ids = [
    7, 4, 5, 9, 10, 12, 6, 1, 2, 3, 13, 14, 15, 16, 17, 18, 19, 24, 20,
    21, 22, 23, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 36, 37, 38, 40,
    43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59,
    60, 61, 64, 68, 69, 70, 66, 35, 39
]

 
failed_repo_ids = [ ]

if __name__ == "__main__":
    # print("Repo Driller")

    for project_key in PROJECTS:
        project = PROJECTS[project_key]
        # project = PROJECTS['capacitor']


        if "progress" in project and project["progress"] == "100":
            repo_path = project["local-repo-dir"]
            repo = Repo(repo_path)


            print(f"\n**** {project['name']} ****")
            ## checkout to our specified revisions
            try:
                assert_checkout(repo, project)
            except: 
                print(f"{project['name']} git ref not as specified for analysis. \n {project['local-repo-dir']}")
                repo.git.checkout(project["commit-n-sha"], force=True)
                assert_checkout(repo, project)

            repo_id = get_repo_from_db(MYSQL_CONNECTION, **project)
            print(f"repo_id {repo_id}")

            if repo_id in completed_repo_ids or repo_id in failed_repo_ids:
                print("skipping.......")
                continue

            if repo_id is None:
                sys.exit("repo_id cannot be null!")


            for commit in Repository(path_to_repo=repo_path).traverse_commits():
                #print(f"inserting commit {commit.hash}")

                last_inserted_commit_id = insert_commit_to_db(
                    MYSQL_CONNECTION,
                    repo_id,
                    sha=commit.hash,
                    message=commit.msg,
                    author=commit.author.name,
                    authored_date=commit.author_date,
                    author_timezone=commit.author_timezone,
                    author_email=commit.author.email,
                    commiter=commit.committer.name,
                    commiter_email=commit.committer.email,
                    commited_date=commit.committer_date,
                    commiter_timezone=commit.committer_timezone,
                    merge=commit.merge,
                    files=commit.files,
                    no_lines=commit.lines,
                    insertions=commit.insertions,
                    deletions=commit.deletions,
                    modified=commit.lines,
                )

                #print("commit id: ", last_inserted_commit_id)

                for parent_hash in commit.parents:
                     insert_commit_parent_to_db(MYSQL_CONNECTION,
                                      commit_sha=commit.hash,
                                      commit_id=last_inserted_commit_id,
                                      parent_sha=parent_hash)


            print(f"** done ** {project['name']} ")