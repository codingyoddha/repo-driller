import pymysql.cursors

HOSTNAME = "localhost"
USERNAME = "root"
PASSWORD = "root"
DATABASE = "git"

MYSQL_CONNECTION = pymysql.connect(
    host=HOSTNAME,
    user=USERNAME,
    password=PASSWORD,
    database=DATABASE
)