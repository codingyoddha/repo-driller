import re
import jellyfish
from pydriller import Repository, ModificationType
from db import MYSQL_CONNECTION
from strictyaml import load
import sys
import os

# PROJECT = "capacitor2"
# PROJECT_NAME = "capacitor"
# BASE_REPO_PATH = f"/Users/prashamojha/prsm/thesis/fgit/{PROJECT}"

FINER_GIT_BASE_PATH = "/Users/prashamojha/prsm/finergit/"


DISTANCE_THRESHOLD_LT_20 = 0.95
DISTANCE_THRESHOLD_GT_20 = 0.85

FROM_EXCLUSIVE_ID = 50
TO_INCLUSIVE_ID = 75

completed_repo_ids = [

]
left_to_do_again = [66]

# 66 - Graal
failed_repo_ids = []


with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data


# @returns return id, already_exists
def get_repo_from_db(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"SELECT id FROM csrepo WHERE name=%s"
        cursor.execute(sql, (kwargs.get("name")))
        if cursor.rowcount == 1:
            return cursor.fetchone()[0]

        raise Exception


def get_file_id_from_db(connection, repo_id, file_path):
    if file_path is None:
        return None

    if repo_id is None:
        raise

    with connection.cursor() as cursor:
        sql = f"SELECT id FROM csfilecommit WHERE repo_id = %s and file_path = %s"
        cursor.execute(sql, (repo_id, file_path))
        if cursor.rowcount > 0:
            return cursor.fetchone()[0]

    return None


def get_commit_id_from_db(connection, repo_id, sha):
    if sha is None:
        return None

    if repo_id is None:
        raise

    with connection.cursor() as cursor:
        sql = f"SELECT id FROM cscommit WHERE repo_id = %s and sha = %s"
        cursor.execute(sql, (repo_id, sha))
        if cursor.rowcount == 1:
            return cursor.fetchone()[0]

    return None


def get_class_shape_id_from_db(connection, repo_id, sha, file_path):
    if file_path is None or sha is None:
        return None

    if repo_id is None:
        raise

    with connection.cursor() as cursor:
        sql = f"SELECT id FROM class_shape WHERE repo_id = %s and file_path = %s and sha = %s"
        cursor.execute(sql, (repo_id, file_path, sha))
        if cursor.rowcount == 1:
            return cursor.fetchone()[0]

    return None


def get_method_shape_from_db(connection, repo_id):
    with connection.cursor() as cursor:
        sql = f"SELECT id, sha, file_path  FROM method_shape WHERE repo_id=%s"
        cursor.execute(sql, repo_id)

        if cursor.rowcount > 0:
            return cursor.fetchall()
        return []


def get_package_shape_from_db(connection, repo_id):
    with connection.cursor() as cursor:
        sql = f"SELECT id, sha, file_path  FROM package_shape WHERE repo_id=%s"
        cursor.execute(sql, repo_id)

        if cursor.rowcount > 0:
            return cursor.fetchall()
        return []


def update_package_shape_to_db(connection, repo_id, package_shape_id, class_shape_id):
    try:
        with connection.cursor() as cursor:
            sql = f"UPDATE package_shape SET class_shape_id = %s WHERE id = %s AND repo_id = %s"
            cursor.execute(
                sql, (
                    class_shape_id,
                    package_shape_id,
                    repo_id
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while update_package_shape_to_db: ", package_shape_id)
        raise


def update_method_shape_to_db(connection, repo_id, method_shape_id, class_shape_id):
    try:
        with connection.cursor() as cursor:
            sql = f"UPDATE method_shape SET class_shape_id = %s WHERE id = %s AND repo_id = %s"
            cursor.execute(
                sql, (
                    class_shape_id,
                    method_shape_id,
                    repo_id
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while update_method_shape_to_db: ", method_shape_id)
        raise


def transform_change_type(change_type):
    if change_type == ModificationType.ADD:
        return "add"
    elif change_type == ModificationType.COPY:
        return "copy"
    elif change_type == ModificationType.RENAME:
        return "rename"
    elif change_type == ModificationType.DELETE:
        return "delete"
    elif change_type == ModificationType.MODIFY:
        return "modify"
    else:
        return "unknown"


def remove_token_identifier(token):
    token_search = re.match(r".*Token\[(.*)\]", token, re.IGNORECASE)

    if token_search:
        return token_search.group(1)
    else:
        print("******* token identified error", token)

    return token


def is_similar(a, b):
    distance = jellyfish.jaro_winkler_similarity(a, b)

    if (len(a) < 20 and distance >= DISTANCE_THRESHOLD_LT_20):
        return True
    elif (len(a) >= 20 and distance >= DISTANCE_THRESHOLD_GT_20):
        return True
    return False


def is_modified(added, deleted):
    if len(deleted) < 1 or len(added) < 1:
        return False

    pass


class TypeOfFile:
    INTERFACE = "interface"
    CLASS = "class"
    METHOD = "method"
    CONSTRUCTOR = "constructor"
    ENUM = "enum"
    PACKAGE = "package"


class TypeOfFileExtension:
    INTERFACE = ".ijava"
    CLASS = ".cjava"
    METHOD = ".mjava"
    CONSTRUCTOR = ".cmjava"
    ENUM = ".ejava"
    PACKAGE = ".pjava"


class Token:
    ANNOTATION = "AnnotationToken"
    MODIFIER = "ModifierToken"
    TYPE = "TypeToken"
    CLASSNAME = "ClassNameToken"
    EXTENDS = "ClassExtendsToken"
    IMPLEMENTS = "ClassImplementsToken"
    FIELD = "FieldToken"
    CONSTRUCTOR = "ConstructorToken"
    METHOD = "MethodToken"
    RETURN = "MethodReturnToken"
    METHODNAME = "MethodNameToken"
    PARAMETER = "ParameterToken"
    EXCEPTION = "ExceptionToken"
    PACKAGE = "PackageToken"
    IMPORT = "ImportToken"
    IMPORTSTATIC = "ImportStaticToken"
    CLASS = "ClassToken"
    ENUM = "EnumToken"
    INTERFACE = "InterfaceToken"
    BODY = "MethodBody"
    SECTIONS = ["[METHOD-PARAM]", "[END]", "[METHOD-EXCEPTION]", "[METHOD-BODY]", "[CLASS_FIELD]",
                "[CLASS-CONSTRUCTOR]", "[CLASS-METHOD]"]


def get_type_of_file(filename):
    if filename.endswith(TypeOfFileExtension.CLASS):
        return TypeOfFile.CLASS
    elif filename.endswith(TypeOfFileExtension.INTERFACE):
        return TypeOfFile.INTERFACE
    elif filename.endswith(TypeOfFileExtension.METHOD):
        return TypeOfFile.METHOD
    elif filename.endswith(TypeOfFileExtension.CONSTRUCTOR):
        return TypeOfFile.CONSTRUCTOR
    elif filename.endswith(TypeOfFileExtension.ENUM):
        return TypeOfFile.ENUM
    elif filename.endswith(TypeOfFileExtension.PACKAGE):
        return TypeOfFile.PACKAGE

    print("Error", filename)
    raise


def get_file_path(change_type, modification):
    if change_type == "add" or change_type == "rename" or change_type == "modify" or change_type == "copy":
        filepath = modification.new_path
    elif change_type == "delete":
        filepath = modification.old_path
    else:
        filepath = modification.old_path

    return filepath


def get_class_shape_token_bucket():
    return {
        Token.ANNOTATION: {
            "added": 0,
            "deleted": 0,
        },
        Token.MODIFIER: {
            "added": 0,
            "deleted": 0,
        },
        Token.TYPE: {
            "added": 0,
            "deleted": 0,
        },
        Token.CLASSNAME: {
            "added": 0,
            "deleted": 0,
        },
        Token.EXTENDS: {
            "added": 0,
            "deleted": 0,
        },
        Token.IMPLEMENTS: {
            "added": 0,
            "deleted": 0,
        },
        Token.FIELD: {
            "added": 0,
            "deleted": 0,
        },
        Token.CONSTRUCTOR: {
            "added": 0,
            "deleted": 0,
        },
        Token.METHOD: {
            "added": 0,
            "deleted": 0,
        },
    }


def get_method_shape_token_bucket():
    return {
        Token.ANNOTATION: {
            "added": 0,
            "deleted": 0,
        },
        Token.MODIFIER: {
            "added": 0,
            "deleted": 0,
        },
        Token.RETURN: {
            "added": 0,
            "deleted": 0,
        },
        Token.METHODNAME: {
            "added": 0,
            "deleted": 0,
        },
        Token.PARAMETER: {
            "added": 0,
            "deleted": 0,
        },
        Token.EXCEPTION: {
            "added": 0,
            "deleted": 0,
        },
        Token.BODY: {
            "added": 0,
            "deleted": 0,
        },
    }


def get_package_shape_token_bucket():
    return {
        Token.PACKAGE: {
            "added": 0,
            "deleted": 0,
        },
        Token.IMPORT: {
            "added": 0,
            "deleted": 0,
        },
        Token.IMPORTSTATIC: {
            "added": 0,
            "deleted": 0,
        },
        Token.CLASS: {
            "added": 0,
            "deleted": 0,
        },
        Token.ENUM: {
            "added": 0,
            "deleted": 0,
        },
        Token.INTERFACE: {
            "added": 0,
            "deleted": 0,
        },
        Token.FIELD: {
            "added": 0,
            "deleted": 0,
        }
    }


def update_token_bucket(token, action, token_bucket):
    def added(token_type):
        if token_type in token_bucket:
            token_bucket[token_type]['added'] += 1

    def deleted(token_type):
        if token_type in token_bucket:
            token_bucket[token_type]['deleted'] += 1

    def update(token_type):
        if (action == "added"):
            added(token_type)
        elif (action == "deleted"):
            deleted(token_type)
        else:
            print("ERROR: no action found? ", action, token_type)
            raise

    if token.startswith(Token.FIELD) and Token.FIELD in token_bucket:
        update(Token.FIELD)

    elif token.startswith(Token.METHOD) and Token.METHOD in token_bucket:
        update(Token.METHOD)

    elif token.startswith(Token.CONSTRUCTOR) and Token.CONSTRUCTOR in token_bucket:
        update(Token.CONSTRUCTOR)

    elif token.startswith(Token.MODIFIER) and Token.MODIFIER in token_bucket:
        update(Token.MODIFIER)

    elif token.startswith(Token.TYPE) and Token.TYPE in token_bucket:
        update(Token.TYPE)

    elif token.startswith(Token.RETURN) and Token.RETURN in token_bucket:
        update(Token.RETURN)

    elif token.startswith(Token.METHODNAME) and Token.METHODNAME in token_bucket:
        update(Token.METHODNAME)

    elif token.startswith(Token.PARAMETER) and Token.PARAMETER in token_bucket:
        update(Token.PARAMETER)

    elif token.startswith(Token.EXCEPTION) and Token.EXCEPTION in token_bucket:
        update(Token.EXCEPTION)

    elif token.startswith(Token.PACKAGE) and Token.PACKAGE in token_bucket:
        update(Token.PACKAGE)

    elif token.startswith(Token.IMPORT) and Token.IMPORT in token_bucket:
        update(Token.IMPORT)

    elif token.startswith(Token.IMPORTSTATIC) and Token.IMPORTSTATIC in token_bucket:
        update(Token.IMPORTSTATIC)

    elif token.startswith(Token.CLASSNAME) and Token.CLASSNAME in token_bucket:
        update(Token.CLASSNAME)

    elif token.startswith(Token.EXTENDS) and Token.EXTENDS in token_bucket:
        update(Token.EXTENDS)

    elif token.startswith(Token.IMPLEMENTS) and Token.IMPLEMENTS in token_bucket:
        update(Token.IMPLEMENTS)

    elif token.startswith(Token.CLASS) and Token.CLASS in token_bucket:
        update(Token.CLASS)

    elif token.startswith(Token.ENUM) and Token.ENUM in token_bucket:
        update(Token.ENUM)

    elif token.startswith(Token.INTERFACE) and Token.INTERFACE in token_bucket:
        update(Token.INTERFACE)

    elif token.startswith(Token.ANNOTATION) and Token.ANNOTATION in token_bucket:
        update(Token.ANNOTATION)

    elif token not in Token.SECTIONS and Token.BODY in token_bucket:
        update(Token.BODY)
    else:
        return False

    return True


def get_added_value(token_type, token_bucket):
    return token_bucket[token_type]['added']


def get_deleted_value(token_type, token_bucket):
    return token_bucket[token_type]['deleted']


def is_token_type_changed(token_type, token_bucket):
    if token_type in token_bucket:
        if token_bucket[token_type]['added'] > 0 or token_bucket[token_type]['deleted'] > 0:
            return True
    return False


def get_type_of_change(token_type, token_bucket):
    token_change_type = {
        Token.ANNOTATION: "annotation",
        Token.TYPE: "type",
        Token.MODIFIER: "modifier",
        Token.CLASSNAME: "classname",
        Token.EXTENDS: "extends",
        Token.IMPLEMENTS: "implements",
        Token.FIELD: "field",
        Token.CONSTRUCTOR: "constructor",
        Token.METHOD: "method",
        Token.RETURN: "return",
        Token.METHODNAME: "methodname",
        Token.PARAMETER: "parameter",
        Token.EXCEPTION: "exception",
        Token.PACKAGE: "package",
        Token.IMPORT: "import",
        Token.IMPORTSTATIC: "importstatic",
        Token.CLASS: "class",
        Token.ENUM: "enum",
        Token.INTERFACE: "interface",
        Token.BODY: "body"
    }

    if is_token_type_changed(token_type, token_bucket):
        return token_change_type[token_type]
    else:
        return None


def type_of_changes(token_bucket):
    class_changes = []
    for token_type in token_bucket:
        change_type_text = get_type_of_change(token_type, token_bucket)
        if change_type_text is not None:
            class_changes.append(change_type_text)
    if len(class_changes) > 0:
        return ",".join(class_changes)
    return None


def insert_class_shape_to_db(connection, repo_id, commit_id, sha, file_id, token_bucket, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = """
            INSERT INTO class_shape
(
 repo_id,
 file_id,
 commit_id,
 sha,
 name,
 extension,
 file_type,
 change_type,
 diff,
 file_path,
 new_path,
 old_path,
 old_path_file_id,
 new_path_file_id,
 changes,
 annotation_added,
 annotation_deleted,
 modifier_added,
 modifier_deleted,
 type_added,
 type_deleted,
 classname_added,
 classname_deleted,
 extends_added,
 extends_deleted,
 implements_added,
 implements_deleted,
 field_added,
 field_deleted,
 constructor_added,
 constructor_deleted,
 method_added,
 method_deleted)
VALUES
            (
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s,
 %s)
            """

            cursor.execute(
                sql, (
                    repo_id,
                    file_id,
                    commit_id,
                    sha,
                    kwargs.get("name"),
                    kwargs.get("extension"),
                    kwargs.get("file_type"),
                    kwargs.get("change_type"),
                    kwargs.get("diff"),
                    kwargs.get("file_path"),
                    kwargs.get("new_path"),
                    kwargs.get("old_path"),
                    kwargs.get("old_path_file_id"),
                    kwargs.get("new_path_file_id"),
                    kwargs.get("changes"),
                    get_added_value(Token.ANNOTATION, token_bucket),
                    get_deleted_value(Token.ANNOTATION, token_bucket),
                    get_added_value(Token.MODIFIER, token_bucket),
                    get_deleted_value(Token.MODIFIER, token_bucket),
                    get_added_value(Token.TYPE, token_bucket),
                    get_deleted_value(Token.TYPE, token_bucket),
                    get_added_value(Token.CLASSNAME, token_bucket),
                    get_deleted_value(Token.CLASSNAME, token_bucket),
                    get_added_value(Token.EXTENDS, token_bucket),
                    get_deleted_value(Token.EXTENDS, token_bucket),
                    get_added_value(Token.IMPLEMENTS, token_bucket),
                    get_deleted_value(Token.IMPLEMENTS, token_bucket),
                    get_added_value(Token.FIELD, token_bucket),
                    get_deleted_value(Token.FIELD, token_bucket),
                    get_added_value(Token.CONSTRUCTOR, token_bucket),
                    get_deleted_value(Token.CONSTRUCTOR, token_bucket),
                    get_added_value(Token.METHOD, token_bucket),
                    get_deleted_value(Token.METHOD, token_bucket),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while insert_class_shape_to_db: ", repo_id, kwargs)
        raise


def insert_package_shape_to_db(connection, repo_id, commit_id, sha, file_id, token_bucket, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = """
INSERT INTO package_shape
(
repo_id,
file_id,
commit_id,
sha,
name,
extension,
file_type,
change_type,
diff,
file_path,
new_path,
old_path,
old_path_file_id,
new_path_file_id,
changes,
package_added,
package_deleted,
import_added,
import_deleted,
importstatic_added,
importstatic_deleted,
class_added,
class_deleted,
enum_added,
enum_deleted,
interface_added,
interface_deleted)
VALUES
(
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s);

"""

            cursor.execute(
                sql, (
                    repo_id,
                    file_id,
                    commit_id,
                    sha,
                    kwargs.get("name"),
                    kwargs.get("extension"),
                    kwargs.get("file_type"),
                    kwargs.get("change_type"),
                    kwargs.get("diff"),
                    kwargs.get("file_path"),
                    kwargs.get("new_path"),
                    kwargs.get("old_path"),
                    kwargs.get("old_path_file_id"),
                    kwargs.get("new_path_file_id"),
                    kwargs.get("changes"),
                    get_added_value(Token.PACKAGE, token_bucket),
                    get_deleted_value(Token.PACKAGE, token_bucket),
                    get_added_value(Token.IMPORT, token_bucket),
                    get_deleted_value(Token.IMPORT, token_bucket),
                    get_added_value(Token.IMPORTSTATIC, token_bucket),
                    get_deleted_value(Token.IMPORTSTATIC, token_bucket),
                    get_added_value(Token.CLASS, token_bucket),
                    get_deleted_value(Token.CLASS, token_bucket),
                    get_added_value(Token.ENUM, token_bucket),
                    get_deleted_value(Token.ENUM, token_bucket),
                    get_added_value(Token.INTERFACE, token_bucket),
                    get_deleted_value(Token.INTERFACE, token_bucket),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while insert_package_shape_to_db: ", repo_id, kwargs)
        raise


def insert_method_shape_to_db(connection, repo_id, commit_id, sha, file_id, token_bucket, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = """
INSERT INTO method_shape
(
repo_id,
file_id,
commit_id,
sha,
name,
longname,
class_shape_name,
class_shape_id,
extension,
file_type,
change_type,
diff,
file_path,
new_path,
old_path,
old_path_file_id,
new_path_file_id,
changes,
annotation_added,
annotation_deleted,
modifier_added,
modifier_deleted,
return_added,
return_deleted,
methodname_added,
methodname_deleted,
parameter_added,
parameter_deleted,
exception_added,
exception_deleted,
body_added,
body_deleted)
VALUES
(
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s,
%s);

"""
            cursor.execute(
                sql, (
                    repo_id,
                    file_id,
                    commit_id,
                    sha,
                    kwargs.get("name"),
                    kwargs.get("longname"),
                    kwargs.get("class_shape_name"),
                    kwargs.get("class_shape_id"),
                    kwargs.get("extension"),
                    kwargs.get("file_type"),
                    kwargs.get("change_type"),
                    kwargs.get("diff"),
                    kwargs.get("file_path"),
                    kwargs.get("new_path"),
                    kwargs.get("old_path"),
                    kwargs.get("old_path_file_id"),
                    kwargs.get("new_path_file_id"),
                    kwargs.get("changes"),
                    get_added_value(Token.ANNOTATION, token_bucket),
                    get_deleted_value(Token.ANNOTATION, token_bucket),
                    get_added_value(Token.MODIFIER, token_bucket),
                    get_deleted_value(Token.MODIFIER, token_bucket),
                    get_added_value(Token.RETURN, token_bucket),
                    get_deleted_value(Token.RETURN, token_bucket),
                    get_added_value(Token.METHODNAME, token_bucket),
                    get_deleted_value(Token.METHODNAME, token_bucket),
                    get_added_value(Token.PARAMETER, token_bucket),
                    get_deleted_value(Token.PARAMETER, token_bucket),
                    get_added_value(Token.EXCEPTION, token_bucket),
                    get_deleted_value(Token.EXCEPTION, token_bucket),
                    get_added_value(Token.BODY, token_bucket),
                    get_deleted_value(Token.BODY, token_bucket),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except Exception as err:
        print("error while insert_package_shape_to_db: ", repo_id, kwargs)
        raise


def get_proper_java_extension_file_path(file_path):
    if file_path is not None and isinstance(file_path, str):
        if file_path.endswith(TypeOfFileExtension.CLASS):
            return file_path.replace(TypeOfFileExtension.CLASS, ".java")
        elif file_path.endswith(TypeOfFileExtension.ENUM):
            return file_path.replace(TypeOfFileExtension.ENUM, ".java")
        elif file_path.endswith(TypeOfFileExtension.INTERFACE):
            return file_path.replace(TypeOfFileExtension.INTERFACE, ".java")
        elif file_path.endswith(TypeOfFileExtension.METHOD):
            return file_path.replace(TypeOfFileExtension.METHOD, ".java")
        elif file_path.endswith(TypeOfFileExtension.CONSTRUCTOR):
            return file_path.replace(TypeOfFileExtension.CONSTRUCTOR, ".java")
        elif file_path.endswith(TypeOfFileExtension.PACKAGE):
            return file_path.replace(TypeOfFileExtension.PACKAGE, ".java")

    return file_path


def update_shape_ids():
    print("update method-shape")
    for (method_shape_id, sha, file_path) in get_method_shape_from_db(MYSQL_CONNECTION, repo_id):
        class_shape_id = get_class_shape_id_from_db(MYSQL_CONNECTION, repo_id, sha, file_path)
        if class_shape_id is not None:
            update_method_shape_to_db(MYSQL_CONNECTION, repo_id, method_shape_id, class_shape_id)

    print("update package-shape")
    for (package_shape_id, sha, file_path) in get_package_shape_from_db(MYSQL_CONNECTION, repo_id):
        class_shape_id = get_class_shape_id_from_db(MYSQL_CONNECTION, repo_id, sha, file_path)
        if class_shape_id is not None:
            update_package_shape_to_db(MYSQL_CONNECTION, repo_id, package_shape_id, class_shape_id)




if __name__ == "__main__":

    for project_key in PROJECTS:
        project = PROJECTS[project_key]
        # project = PROJECTS[PROJECT_NAME]
        repo_id = get_repo_from_db(MYSQL_CONNECTION, name=project['name'])

        if repo_id is None:
            sys.exit("repo_id cannot be null!")

        if not(repo_id > FROM_EXCLUSIVE_ID and repo_id <= TO_INCLUSIVE_ID):
            print(f"..no within the range {repo_id} processing ..")
            continue

        if repo_id in completed_repo_ids or repo_id in failed_repo_ids:
            print("skipping already ......")
            continue

        if "progress" in project and project["progress"] == "100":
            src_path = project["local-repo-dir"]
            dst_path = project["finer-repo-dir"]

            dst_full_path = os.path.join(FINER_GIT_BASE_PATH, dst_path)

            print(f"\n**** {project['name']} ({repo_id} ****")
            print(dst_full_path)

            for commit in Repository(path_to_repo=dst_full_path, order="reverse").traverse_commits():
                original_commit_sha = commit.msg
                # print(original_commit_sha)
                commit_id = get_commit_id_from_db(MYSQL_CONNECTION, repo_id, original_commit_sha)

                for modification in commit.modified_files:
                    type_of_file = get_type_of_file(modification.filename)
                    change_type = transform_change_type(modification.change_type)

                    if type_of_file == TypeOfFile.CLASS or type_of_file == TypeOfFile.INTERFACE or type_of_file == TypeOfFile.ENUM:
                        filename = get_proper_java_extension_file_path(modification.filename)
                        filepath = get_file_path(change_type, modification)
                        filepath = get_proper_java_extension_file_path(filepath)
                        old_path = get_proper_java_extension_file_path(modification.old_path)
                        new_path = get_proper_java_extension_file_path(modification.new_path)

                        if type_of_file == TypeOfFile.CLASS:
                            extension = TypeOfFileExtension.CLASS
                        elif type_of_file == TypeOfFile.INTERFACE:
                            extension = TypeOfFileExtension.INTERFACE
                        elif type_of_file == TypeOfFile.ENUM:
                            extension = TypeOfFileExtension.ENUM

                        file_id = get_file_id_from_db(MYSQL_CONNECTION, repo_id, filepath)
                        old_path_file_id = get_file_id_from_db(
                            MYSQL_CONNECTION, repo_id, old_path
                        )
                        new_path_file_id = get_file_id_from_db(
                            MYSQL_CONNECTION, repo_id, new_path
                        )

                        token_bucket = get_class_shape_token_bucket()

                        for idx, token in modification.diff_parsed['added']:
                            update_token_bucket(token, "added", token_bucket)

                        for idx, token in modification.diff_parsed['deleted']:
                            update_token_bucket(token, "deleted", token_bucket)

                        insert_class_shape_to_db(
                            MYSQL_CONNECTION,
                            repo_id=repo_id,
                            commit_id=commit_id,
                            sha=original_commit_sha,
                            file_id=file_id,
                            token_bucket=token_bucket,
                            name=filename,
                            extension=extension,
                            change_type=change_type,
                            file_type=type_of_file,
                            diff=modification.diff,
                            file_path=filepath,
                            new_path=new_path,
                            old_path=old_path,
                            changes=type_of_changes(token_bucket),
                            old_path_file_id=old_path_file_id,
                            new_path_file_id=new_path_file_id,
                        )

                    elif type_of_file == TypeOfFile.METHOD or type_of_file == TypeOfFile.CONSTRUCTOR:

                        longname = modification.filename
                        # AndroidProtocolHandler#AndroidProtocolHandler(Context).cmjava
                        split_name = modification.filename.split("#", 1)
                        class_shape_name = split_name[0]
                        class_shape_name = class_shape_name + ".java"
                        method_name = split_name[1]
                        method_name = method_name.replace(TypeOfFileExtension.METHOD, "")

                        filepath = get_file_path(change_type, modification)
                        filepath = filepath.split("#", 1)[0]
                        filepath = filepath + ".java"

                        old_path = None
                        new_path = None
                        if modification.old_path is not None:
                            old_path = modification.old_path.split("#", 1)[0]
                            old_path = old_path + ".java"

                        if modification.new_path is not None:
                            new_path = modification.new_path.split("#", 1)[0]
                            new_path = new_path + ".java"

                        if type_of_file == TypeOfFile.METHOD:
                            extension = TypeOfFileExtension.METHOD
                        elif type_of_file == TypeOfFile.CONSTRUCTOR:
                            extension = TypeOfFileExtension.CONSTRUCTOR

                        file_id = get_file_id_from_db(MYSQL_CONNECTION, repo_id, filepath)
                        old_path_file_id = get_file_id_from_db(
                            MYSQL_CONNECTION, repo_id, old_path
                        )
                        new_path_file_id = get_file_id_from_db(
                            MYSQL_CONNECTION, repo_id, new_path
                        )

                        token_bucket = get_method_shape_token_bucket()

                        for idx, token in modification.diff_parsed['added']:
                            update_token_bucket(token, "added", token_bucket)

                        for idx, token in modification.diff_parsed['deleted']:
                            update_token_bucket(token, "deleted", token_bucket)

                        if token_bucket[Token.BODY]['added'] > 0:
                            token_bucket[Token.BODY]['added'] = 1

                        if token_bucket[Token.BODY]['deleted'] > 0:
                            token_bucket[Token.BODY]['deleted'] = 1

                        insert_method_shape_to_db(
                            MYSQL_CONNECTION,
                            repo_id=repo_id,
                            commit_id=commit_id,
                            sha=original_commit_sha,
                            file_id=file_id,
                            token_bucket=token_bucket,
                            name=method_name,
                            longname=longname,
                            class_shape_name=class_shape_name,
                            class_shape_id=None,
                            extension=extension,
                            change_type=change_type,
                            file_type=type_of_file,
                            diff=modification.diff,
                            file_path=filepath,
                            new_path=new_path,
                            old_path=old_path,
                            changes=type_of_changes(token_bucket),
                            old_path_file_id=old_path_file_id,
                            new_path_file_id=new_path_file_id,
                        )

                    elif type_of_file == TypeOfFile.PACKAGE:
                        filename = get_proper_java_extension_file_path(modification.filename)
                        filepath = get_file_path(change_type, modification)
                        filepath = get_proper_java_extension_file_path(filepath)
                        old_path = get_proper_java_extension_file_path(modification.old_path)
                        new_path = get_proper_java_extension_file_path(modification.new_path)

                        extension = TypeOfFileExtension.PACKAGE

                        file_id = get_file_id_from_db(MYSQL_CONNECTION, repo_id, filepath)
                        old_path_file_id = get_file_id_from_db(
                            MYSQL_CONNECTION, repo_id, old_path
                        )
                        new_path_file_id = get_file_id_from_db(
                            MYSQL_CONNECTION, repo_id, new_path
                        )
                        token_bucket = get_package_shape_token_bucket()

                        for idx, token in modification.diff_parsed['added']:
                            update_token_bucket(token, "added", token_bucket)

                        for idx, token in modification.diff_parsed['deleted']:
                            update_token_bucket(token, "deleted", token_bucket)

                        insert_package_shape_to_db(
                            MYSQL_CONNECTION,
                            repo_id=repo_id,
                            commit_id=commit_id,
                            sha=original_commit_sha,
                            file_id=file_id,
                            token_bucket=token_bucket,
                            name=filename,
                            extension=extension,
                            change_type=change_type,
                            file_type=type_of_file,
                            diff=modification.diff,
                            file_path=filepath,
                            new_path=new_path,
                            old_path=old_path,
                            changes=type_of_changes(token_bucket),
                            old_path_file_id=old_path_file_id,
                            new_path_file_id=new_path_file_id,
                        )

