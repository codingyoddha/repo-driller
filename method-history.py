from git import Repo
import os
import pymysql.cursors
from halo import Halo
import requests
import json
import time
import sys
from requests.models import ReadTimeoutError
from strictyaml import load
import argparse
import traceback
from datetime import datetime


parser = argparse.ArgumentParser(description='Method history extractor')

parser.add_argument('mode', type=str, help='Actions to perform')


parser.add_argument('mine', type=str, nargs='*', help='Name of the project')
parser.add_argument('--timeout', type=int, help='Request timeout for codeshovel')
parser.add_argument('--retries', type=int, help='Retries for the program')
parser.add_argument('--start-with', type=int, help='Start position for the method id (inclusive)')
parser.add_argument('--end-with', type=int, help='End position for the method id (inclusive) ')
parser.add_argument('--start-with-file', type=int, help='Start position for the file id (inclusive)')
parser.add_argument('--end-with-file', type=int, help='End position for the file id (inclusive) ')
parser.add_argument('--file-limit', type=int, help='No of records file (limit) sql')
parser.add_argument('--file-offset', type=int, help='Offset position for file (offset) sql ')
parser.add_argument('--only-files', action='store_true', help='Scan files and store in db. ')
parser.add_argument('--only-methods', action='store_true', help='Scan methods and store in db.')
parser.add_argument('--force-files', action='store_true', help='Scan files again and store in db. ')
parser.add_argument('--no-hostname', action='store_true', help='Use proper URL')
parser.add_argument('--file-reverse', action='store_true', help='Reverse file order by id when file range is used')


args = parser.parse_args()

#print(args)

with open("./projects.yaml", 'r') as file:
    PROJECTS = load(file.read()).data

with open("./skipped-files.yaml", 'r') as file:
    SKIPPED_FILES = load(file.read()).data

MYSQL_CONN = pymysql.connect(
    host='localhost',
    user='root',
    password='root',
    database='git'
)


if args.mode == 'list':
    for project in PROJECTS:
        print(project)
    sys.exit()

if args.mode == 'list-finished':
    for project in PROJECTS:
        if 'progress' in PROJECTS[project] and PROJECTS[project]['progress'] == '100':
            print(project)
    sys.exit()

if args.mode == 'list-remaining':
    for project in PROJECTS:
        if not 'progress' in PROJECTS[project] or PROJECTS[project]['progress'] != '100':
            print(project)
    sys.exit()


UPDATE_PROJECTS = False
if args.mode == "update-projects":
    UPDATE_PROJECTS = True

    def update_repo_progress_to_db(connection, name, progress):
        with connection.cursor() as cursor:
            sql = f"SELECT id FROM csrepo WHERE name=%s"
            cursor.execute(sql, (name))
            if cursor.rowcount != 1:
                sys.exit(f"project with name {name} not found in db or multiple entry!. Fix it first")
            id = cursor.fetchone()[0]

            sql = f"UPDATE csrepo SET progress = %s WHERE id = %s"
            cursor.execute(
                    sql, (
                        progress, id
                    )
                )
            connection.commit()
            return cursor.lastrowid
    
    if UPDATE_PROJECTS:
        for project in PROJECTS:
            if 'progress' in PROJECTS[project] and PROJECTS[project]['progress'] == '100':
                update_repo_progress_to_db(MYSQL_CONN, PROJECTS[project]['name'], PROJECTS[project]['progress'])

        sys.exit("update project finished")

    sys.exit()


if args.mode != 'mine':
    print(args.mode)
    sys.exit("Unfamiliar mode detected. Accepted values are \n list \n mine")   

if args.mode == 'mine' and len(args.mine) < 2:
    sys.exit("Mine requires atleast the project and codeshovel endpoint.")


if args.start_with is not None:
    if args.end_with is None:
        sys.exit("Range start must have a corresponding range end.")
    if args.end_with < args.start_with:
        sys.exit("end-with must be greater than start-with.")

if args.file_limit is not None:
    if args.file_offset is None:
        sys.exit("Limit must have an offset for file")


with open("./ip.json", "r") as file:
    data = json.load(file)
hostnames = {}
for key in data:
    for value in key:
        hostnames[value] = "http://" + key[value]
hostnames["local-1"] =  "http://localhost:4001"
hostnames["local-2"] =  "http://localhost:4002"
hostnames["local-3"] =  "http://localhost:4003"
hostnames["local-4"] =  "http://"
hostnames["prsm-9"]  =  "http://"
hostnames["shovel"]  =  "https://se.cs.ubc.ca/CodeShovel/"
del hostnames["log-streamer"]
print(hostnames)





REQUEST_TIMEOUT = 90 if args.timeout is None else args.timeout
PROGRAM_RETRIES = 10 if args.retries is None else args.retries
PROJECT_ARG = args.mine[0]
CODESHOVEL_ENDPOINT = args.mine[1] if args.no_hostname else hostnames[args.mine[1]]
if not CODESHOVEL_ENDPOINT.endswith("/"):
    CODESHOVEL_ENDPOINT += "/"
 
IS_LOCAL = True if "http://localhost" in CODESHOVEL_ENDPOINT else False
CODESHOVEL_SLEEP_TIMEOUT = 3
CODESHOVEL_200_ERROR_SLEEP_TIMEOUT = 5 if IS_LOCAL else 10
PROGRAM_SLEEP_TIMEOUT = 30
CODESHOVEL_LOCAL_PATH = "/Users/prashamojha/prsm/codeshovel/cache/"
TARGET_FILE_EXTENSION = ".java"

RANGED_MINING = True if args.start_with is not None else False
RANGE_START_WITH = args.start_with if RANGED_MINING else -1
RANGE_END_WITH = args.end_with if RANGED_MINING else -1

FILE_RANGED_MINING = True if args.start_with_file is not None else False
FILE_RANGE_START_WITH = args.start_with_file if FILE_RANGED_MINING else -1
FILE_RANGE_END_WITH = args.end_with_file if FILE_RANGED_MINING else -1
FILE_IS_REVERSE_ORDER = args.file_reverse


FILE_LIMIT_MINING = True if args.file_limit is not None else False
FILE_LIMIT = args.file_limit if FILE_LIMIT_MINING else ""
FILE_OFFSET = args.file_offset if FILE_LIMIT_MINING else ""

FILE_LIMIT_MINING = False if FILE_RANGED_MINING else FILE_LIMIT_MINING

ONLY_FILES = args.only_files 
ONLY_METHODS = args.only_methods
FORCE_FILES = args.force_files






class CodeShovelMethodFailedAgainException(Exception):
    pass

class CheckoutException(Exception):
    pass

class Not200Exception(Exception):
    pass


def checkout(project):
    repo = Repo(project["local-repo-dir"])
    try:
        assert not repo.bare
        assert repo.active_branch.name == project["main-branch"]
        assert repo.active_branch.commit.hexsha == project["commit-n-sha"]
    except:
        raise CheckoutException

def list_files(project, target_file_extension):
    for root, dirs, files in os.walk(project["local-repo-dir"]):
        for file in files:
            if file.endswith(target_file_extension):
                yield file, os.path.join(root, file).replace(project["local-repo-dir"], "")


# @returns return id, already_exists
def insert_repo_to_db(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"SELECT id FROM csrepo WHERE name=%s"
        cursor.execute(sql, (kwargs.get("name")))
        if cursor.rowcount > 0:
            return cursor.fetchone()[0], True

        sql = f"INSERT INTO csrepo (name, url, `git_url`, `local-repo-dir`, `main-branch`, `commit-n-sha`, `commit-n-date`) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(
            sql, (
                kwargs.get("name"),
                kwargs.get("url", None),
                kwargs.get("git-url", None),
                kwargs.get("local-repo-dir"),
                kwargs.get("main-branch"),
                kwargs.get("commit-n-sha", None),
                kwargs.get("commit-n-date", None),
            )
        )
        connection.commit()
        return cursor.lastrowid, False




def insert_file_to_db(connection, repo_id, **kwargs):
    if repo_id is None:
        raise

    try:
        with connection.cursor() as cursor:
            sql = f"SELECT id FROM csfile WHERE repo_id = %s and file_path = %s"
            cursor.execute(sql, (repo_id, kwargs.get("file_path")))
            if cursor.rowcount > 0:
                print(f"File {kwargs.get('file_path')} already in db.")
                return cursor.fetchone()[0]

        with connection.cursor() as cursor:
            sql = f"INSERT INTO csfile (repo_id, file_name, file_path) VALUES (%s, %s, %s)"
            cursor.execute(
                sql, (
                    repo_id,
                    kwargs.get("file_name"),
                    kwargs.get("file_path"),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while insert_file_to_db: ", repo_id, kwargs)
        raise


def update_file_processed_to_db(connection, file_id):
    try:
        with connection.cursor() as cursor:
            sql = f"UPDATE csfile SET processed = 1 WHERE id = %s"
            cursor.execute(
                sql, (
                    file_id,
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while update_file_processed_to_db: ", file_id)
        raise


def update_file_skipped_to_db(connection, file_id):
    try:
        with connection.cursor() as cursor:
            sql = f"UPDATE csfile SET skipped = 1 WHERE id = %s"
            cursor.execute(
                sql, (
                    file_id,
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while update_file_skipped_to_db: ", file_id)
        raise


def insert_method_to_db(connection, repo_id, file_id, **kwargs):
    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO csmethod(repo_id, file_id, name, long_name, isStatic, isAbstract, visibility, start_line) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(
                sql, (
                    repo_id,
                    file_id,
                    kwargs.get("name"),
                    kwargs.get("long_name"),
                    kwargs.get("isStatic"),
                    kwargs.get("isAbstract"),
                    kwargs.get("visibility"),
                    kwargs.get("start_line"),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while insert_method_to_db: ", repo_id, file_id, kwargs)
        raise


def update_method_processed_to_db(connection, method_id):
    try:
        with connection.cursor() as cursor:
            sql = f"UPDATE csmethod SET processed = 1 WHERE id = %s"
            cursor.execute(
                sql, (
                    method_id,
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while update_method_processed_to_db: ", method_id)
        raise


def update_method_skipped_to_db(connection, method_id):
    try:
        with connection.cursor() as cursor:
            sql = f"UPDATE csmethod SET skipped = 1 WHERE id = %s"
            cursor.execute(
                sql, (
                    method_id,
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while update_method_skipped_to_db: ", method_id)
        raise


def insert_method_change_to_db(connection, method_id, **kwargs):
    try:
        with connection.cursor() as cursor:
            sql = f"INSERT INTO csmethodchange" \
                  f"(method_id, type, commit_message, commit_date, commit_name, commit_author, commit_date_old, commit_name_old, commit_author_old, days_between_commits, commits_between_for_repo, commits_between_for_file, diff, extended_details ) " \
                  f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
            cursor.execute(
                sql, (
                    method_id,
                    kwargs.get("type"),
                    kwargs.get("commitMessage"),
                    kwargs.get("commitDate"),
                    kwargs.get("commitName"),
                    kwargs.get("commitAuthor"),
                    kwargs.get("commitDateOld"),
                    kwargs.get("commitNameOld"),
                    kwargs.get("commitAuthorOld"),
                    kwargs.get("daysBetweenCommits"),
                    kwargs.get("commitsBetweenForRepo"),
                    kwargs.get("commitsBetweenForFile"),
                    kwargs.get("diff"),
                    json.dumps(kwargs.get("extendedDetails")),
                )
            )
            connection.commit()
            return cursor.lastrowid
    except:
        print("error while insert_method_change_to_db: ", method_id, kwargs)
        raise

def get_files_from_db(connection, repo_id, check_processed=True):
    with connection.cursor() as cursor:
        if check_processed:
            if FILE_LIMIT_MINING:
                sql = f"SELECT id, repo_id, file_name, file_path FROM csfile WHERE repo_id=%s AND processed != true AND skipped != true AND errors != true ORDER BY id LIMIT {FILE_LIMIT} OFFSET {FILE_OFFSET}"
            elif FILE_RANGED_MINING:
                sql = f"SELECT id, repo_id, file_name, file_path FROM csfile WHERE repo_id=%s AND processed != true AND skipped != true AND errors != true AND id BETWEEN {FILE_RANGE_START_WITH} AND {FILE_RANGE_END_WITH}"
            else:
                sql = f"SELECT id, repo_id, file_name, file_path FROM csfile WHERE repo_id=%s AND processed != true AND skipped != true AND errors != true"
        # GET procssed files
        else:
            if FILE_LIMIT_MINING:
                sql = f"SELECT id, repo_id, file_name, file_path FROM csfile WHERE repo_id=%s AND errors != true AND skipped != true ORDER BY id LIMIT {FILE_LIMIT} OFFSET {FILE_OFFSET}"
            elif FILE_RANGED_MINING:
                if FILE_IS_REVERSE_ORDER:
                    sql = f"SELECT id, repo_id, file_name, file_path FROM csfile WHERE repo_id=%s AND processed = true AND skipped != true AND errors != true AND id BETWEEN {FILE_RANGE_START_WITH} AND {FILE_RANGE_END_WITH} ORDER BY id DESC"
                else:
                    sql = f"SELECT id, repo_id, file_name, file_path FROM csfile WHERE repo_id=%s AND processed = true AND skipped != true AND errors != true AND id BETWEEN {FILE_RANGE_START_WITH} AND {FILE_RANGE_END_WITH} ORDER BY id ASC"
            else:
                sql = f"SELECT id, repo_id, file_name, file_path FROM csfile WHERE repo_id=%s AND errors != true AND skipped != true"

        cursor.execute(sql, repo_id)
        if cursor.rowcount > 0:
            print("row count: files => ", cursor.rowcount)
            # return cursor.fetchmany(3)
            return cursor.fetchall()
        return []


def get_methods_from_db(connection, file_id):
    with connection.cursor() as cursor:
        if RANGED_MINING:
            sql = f"SELECT * FROM csmethod WHERE file_id=%s AND processed != true and skipped != true and parse_errors != true AND id BETWEEN {RANGE_START_WITH} AND {RANGE_END_WITH}"
        else:
            sql = f"SELECT * FROM csmethod WHERE file_id=%s AND processed != true and skipped != true and parse_errors != true"
        cursor.execute(sql, file_id)

        if cursor.rowcount > 0:
            print("row count: methods: => ", cursor.rowcount)
            return cursor.fetchall()
        return []


def clean_up_methods(connection):
    with connection.cursor() as cursor:
        sql = "DELETE FROM csmethod WHERE file_id in (SELECT id FROM csfile WHERE processed != true )"
        cursor.execute(sql)
        connection.commit()


def clean_up_method_history(connection):
    with connection.cursor() as cursor:
        sql = "DELETE FROM csmethodchange WHERE method_id in (SELECT id FROM csmethod WHERE processed != true )"
        cursor.execute(sql)
        connection.commit()


def get_methods_from_codeshovel(project, file_path):
    url = CODESHOVEL_ENDPOINT + "listMethods"
    url += "?gitUrl=" + project['git-url']
    url += "&filePath=" + file_path
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}

    #print(url)
    def get():
        try:
            req = requests.get(url, headers=headers, timeout=REQUEST_TIMEOUT)
            if req.status_code == 200:
                return req.json()
            # if not IS_LOCAL:
            #     re_url = CODESHOVEL_ENDPOINT[:-1] + ":5000"
            #     print(f"\n **** [{datetime.now().time()}] Attempting to restart: ", re_url)
            #     time.sleep(3)
            #     req2 = requests.get(re_url)
            #     if req2.status_code == 200:
            #         print(f"\n **** [{datetime.now().time()}] Restarted docker-compose.")
            #     time.sleep(10)
            raise Not200Exception
        except ConnectionError as err:
            raise err

    try:
        return get()
    except Exception as err:
        print(f"\n**** [{datetime.now().time()}] CodeShovel failed. Sleeping for {CODESHOVEL_SLEEP_TIMEOUT} secs and retrying.", file_path)
        traceback.print_exc()
        time.sleep(CODESHOVEL_SLEEP_TIMEOUT)
        try:
            return get()
        except:
            raise CodeShovelMethodFailedAgainException


def get_method_history_from_codeshovel(project, file_path, method_name, start_line):
    url = CODESHOVEL_ENDPOINT + "getHistory"
    url += "?gitUrl=" + project['git-url']
    url += "&filePath=" + file_path
    url += "&methodName=" + method_name
    url += "&startLine=" + start_line
    #print(url)
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}

    def get():
        try:
            time.sleep(0.5)
            print(f"\n processing {file_path} & {method_name}")
            req = requests.get(url, headers=headers, timeout=REQUEST_TIMEOUT)
            if req.status_code == 200:
                return req.json()
            print("\nStatus Code: ", req.status_code)
            if not IS_LOCAL:
                re_url = CODESHOVEL_ENDPOINT[:-1] + ":5000"
                print(f"\n **** [{datetime.now().time()}] Attempting to restart: ", re_url)
                time.sleep(1)
                req2 = requests.get(re_url)
                if req2.status_code == 200:
                    print(f"\n **** [{datetime.now().time()}] Restarted docker-compose.")
                #time.sleep(5)
            raise Not200Exception
        except (requests.exceptions.ReadTimeout, ReadTimeoutError, TimeoutError) as err:
            raise err
        except ConnectionError as err:
            raise err

    try:
        return get()
    except Not200Exception as err:
        print(f"\n **** [{datetime.now().time()}] Status NOT 200. Skipping and sleeping for {CODESHOVEL_200_ERROR_SLEEP_TIMEOUT} secs")
        time.sleep(CODESHOVEL_200_ERROR_SLEEP_TIMEOUT)
        #try:
            #return get()
        #except:
        raise CodeShovelMethodFailedAgainException
    except (requests.exceptions.ReadTimeout, ReadTimeoutError,TimeoutError) as err:
        print(f"\n**** [{datetime.now().time()}] CodeShovel timeout error.")
        print(f"\n {url}")
        raise CodeShovelMethodFailedAgainException
    except Exception as err:
        print(f"\n**** [{datetime.now().time()}] CodeShovel failed. Sleeping for {CODESHOVEL_SLEEP_TIMEOUT} secs and retrying.",
              file_path, method_name)
        traceback.print_exc()
        time.sleep(CODESHOVEL_SLEEP_TIMEOUT)
        try:
            return get()
        except:
            raise CodeShovelMethodFailedAgainException


def steps(db_connection, project):
    repo_id, already_exists = insert_repo_to_db(db_connection, **project)

    def store_files():
        checkout(project)
        file_counter = 0
        with Halo(
                text=f"Finding and storing files with {TARGET_FILE_EXTENSION} extension from project {project['name']}",
                spinner='dots'
        ):
            for file in list_files(project, TARGET_FILE_EXTENSION):
                insert_file_to_db(db_connection, repo_id,
                                  file_name=file[0], file_path=file[1])
                file_counter += 1
        print(
            f"✅ stored {file_counter} files with {TARGET_FILE_EXTENSION} for project {project['name']}")

    def store_methods():
        method_counter = 0
        with Halo(
                text=f"Getting methods for each file and storing them",
                spinner='dots'
        ):
            for (file_id, _, file_name, file_path) in get_files_from_db(db_connection, repo_id):
                if project["name"] in SKIPPED_FILES and file_path in SKIPPED_FILES[project["name"]]:
                    print("\n ==> skipped: ", file_path)
                    update_file_skipped_to_db(db_connection, file_id)
                    continue
                try:
                    methods = get_methods_from_codeshovel(project, file_path)
                except CodeShovelMethodFailedAgainException as err:
                    update_file_skipped_to_db(db_connection, file_id)
                    continue

                for method in methods:
                    insert_method_to_db(
                        MYSQL_CONN,
                        repo_id,
                        file_id,
                        name=method["methodName"],
                        long_name=method["longName"],
                        start_line=method["startLine"],
                        isStatic=method["isStatic"],
                        isAbstract=method["isAbstract"],
                        visibility=method["visibility"]
                    )
                    method_counter += 1
                update_file_processed_to_db(db_connection, file_id)
                print(f"\n [{datetime.now().time()}] {file_name} processed.")

        print(
            f"✅ Stored {method_counter} methods for project {project['name']} that were not already added.")

    def store_method_history():
        with Halo(
                text=f"Getting method history for each method and storing them",
                spinner='dots'
        ):
            for (file_id, _, file_name, file_path) in get_files_from_db(db_connection, repo_id, False):
                for method_id, _, _, method_name, *method, start_line in get_methods_from_db(db_connection, file_id):
                    try:
                        changes = get_method_history_from_codeshovel(
                            project, file_path, method_name, str(start_line))
                    except CodeShovelMethodFailedAgainException as err:
                        print("\n skipped ==> ", file_path, method_name)
                        update_method_skipped_to_db(db_connection, method_id)
                        continue
                    for sha in changes:
                        insert_method_change_to_db(
                            db_connection, method_id, **changes[sha])
                    update_method_processed_to_db(db_connection, method_id)
                    print(f"\n [{datetime.now().time()}] {method_name} processed. ")
        print(f"✅ Stored method history for project {project['name']}")

    if not already_exists or FORCE_FILES:
        store_files()
    else:
        print(
            f"✅ Project {project['name']} already exists. Skipping file search.")
    
    if ONLY_FILES:
        sys.exit(f"✅ Finished with files.")
    print(f"✅ Cleaning up methods.")
    clean_up_methods(db_connection)
    store_methods()
    if ONLY_METHODS:
        sys.exit(f"✅ Finished with methods.")
    print(f"✅ Cleaning up method history.")
    clean_up_method_history(db_connection)
    store_method_history()





if __name__ == "__main__":
    print("Repo Driller")

    print("Codeshovel endpoint: ", CODESHOVEL_ENDPOINT)
    print(" *** Ranged mining: ", RANGED_MINING)
    print(" *** File Ranged mining: ", FILE_RANGED_MINING)
    print(" *** File limit mining: ", FILE_LIMIT_MINING)

    retries = PROGRAM_RETRIES
    while retries > 0:
        try:
            if PROJECT_ARG in PROJECTS:
                steps(MYSQL_CONN, PROJECTS[PROJECT_ARG])
            else:
                print(f"Project {PROJECT_ARG} not found.")
            retries = 0
        except CheckoutException as err:
            print("*** checkout error ***", err)
            traceback.print_exc()
            sys.exit()
        except Exception as err:
            print(f"*** [{datetime.now().time()}] Exception in main.***", err)
            traceback.print_exc()
            retries -= 1
            time.sleep(PROGRAM_SLEEP_TIMEOUT)
            print(f"Retrying the program. Retries left {retries}")
