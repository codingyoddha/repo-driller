import lizard

diff = """
@@ -1,13 +1,13 @@
     public void getCoreConfigValues() {
         assertTrue(config.isMixedContentAllowed());
         assertArrayEquals(new String[] { "http://www.google.com" }, config.getAllowNavigation());
         assertEquals("test", config.getAndroidScheme());
         assertTrue(config.isInputCaptured());
-        assertTrue(config.isLogsHidden());
+        assertTrue(config.isLoggingEnabled());
         assertFalse(config.isHTML5Mode());
         assertEquals("test-user-agent", config.getOverriddenUserAgentString());
         assertEquals("test-append", config.getAppendedUserAgentString());
         assertTrue(config.isWebContentsDebuggingEnabled());
         assertEquals("red", config.getBackgroundColor());
         assertEquals("http://www.google.com", config.getServerUrl());
     }
\ No newline at end of file
yyy
"""

d  = """@@ -1,9 +1,13 @@
   public JSObject getObject(String name, JSObject defaultValue) {
     Object value = this.data.opt(name);
     if(value == null) { return defaultValue; }
 
     if(value instanceof JSONObject) {
-      return (JSObject) value;
+      try {
+        return JSObject.fromJSONObject((JSONObject) value);
+      } catch (JSONException ex) {
+        return defaultValue;
+      }
     }
     return defaultValue;
   }
\ No newline at end of file

"""

e = """
@@ -0,0 +1,19 @@
+  private String readAssetStream(InputStream stream) {
+    try {
+      final int bufferSize = 1024;
+      final char[] buffer = new char[bufferSize];
+      final StringBuilder out = new StringBuilder();
+      Reader in = new InputStreamReader(stream, "UTF-8");
+      for (; ; ) {
+        int rsz = in.read(buffer, 0, buffer.length);
+        if (rsz < 0)
+          break;
+        out.append(buffer, 0, rsz);
+      }
+      return out.toString();
+    } catch (Exception e) {
+      Log.e(Bridge.TAG, "Unable to process HTML asset file. This is a fatal error", e);
+    }
+
+    return "";
+  }
\ No newline at end of file

"""

def merge(diff):
    output = []
    for line in diff.splitlines():
        if '@@ -' in line:
            continue
        elif '\ No newline' in line:
            continue
        elif '+' in line:
            output.append(line.strip('+'))
        elif not '-' in line:
            output.append(line.strip())
        
    print(output)
    return "\n".join(output)

if __name__ == "__main__":
    print(merge(diff))
    i = lizard.analyze_file.analyze_source_code("x.java", merge(e))
    print(i.__dict__)
    print(i.function_list[0].__dict__)
