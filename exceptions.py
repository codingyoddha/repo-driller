class CodeShovelMethodFailedAgainException(Exception):
    pass

class CheckoutException(Exception):
    pass

class Not200Exception(Exception):
    pass
