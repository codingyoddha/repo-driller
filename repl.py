from pydriller import Repository, ModificationType
import pymysql.cursors
import os
import json

BASE_REPO_PATH = "../repos/"


def get_repos():
    # build up the list of repos
    repos = [
        "jmeter"
    ]

    return [os.path.join(BASE_REPO_PATH, repo) for repo in repos]
    #return ["./"]


def insert_repo(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO repo (name, url) VALUES (%s, %s)"
        cursor.execute(
            sql,
            (kwargs.get("name"), kwargs.get("url", ""))
        )
        #connection.commit()
        return cursor.lastrowid


def insert_file(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO file (commit_id, name, extension, change_type, deleted_lines, added_lines, old_path, new_path, source_code, source_code_before, token_count) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        cursor.execute(
            sql, (
                kwargs.get("commit_id"),
                kwargs.get("name", ""),
                kwargs.get("extension", ""),
                kwargs.get("change_type", ""),
                kwargs.get("deleted_lines", ""),
                kwargs.get("added_lines", ""),
                kwargs.get("old_path", ""),
                kwargs.get("new_path", ""),
                kwargs.get("source_code", ""),
                kwargs.get("source_code_before", ""),
                kwargs.get("token_count", ""),
            )
        )
        #connection.commit()
        return cursor.lastrowid


def insert_commit(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO commit (repo_id, hash, author, email, timestamp, message, deletions, files, insertions, no_lines, merge ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        cursor.execute(
            sql, (
                kwargs.get("repo_id"),
                kwargs.get("hash"),
                kwargs.get("author", ""),
                kwargs.get("email", ""),
                kwargs.get("timestamp", ""),
                kwargs.get("message", ""),
                kwargs.get("deletions", ""),
                kwargs.get("files", ""),
                kwargs.get("insertions", ""),
                kwargs.get("no_lines", ""),
                kwargs.get("merge", ""),
            )
        )
        #connection.commit()
        return cursor.lastrowid


def insert_commit_parents(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO commit_relation (commit_hash, commit_id, parent_hash) VALUES (%s, %s, %s )"

        cursor.execute(
            sql, (
                kwargs.get("commit_hash"),
                kwargs.get("commit_id", ""),
                kwargs.get("parent_hash")
            )
        )
        #connection.commit()
        return cursor.lastrowid


def insert_method(connection, **kwargs):
    with connection.cursor() as cursor:
        sql = f"INSERT INTO method (file_id, name, filename, long_name, length, nloc, end_line, " \
              f"start_line, token_count, top_nesting_level, unit_complexity_low_risk_threshold, " \
              f"unit_interfacing_low_risk_threshold, unit_size_low_risk_threshold, parameters, " \
              f"fan_in, fan_out, general_fan_out, complexity) " \
              f"VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        cursor.execute(
            sql, (
                kwargs.get("file_id"),
                kwargs.get("name"),
                kwargs.get("filename"),
                kwargs.get("long_name"),
                kwargs.get("length"),
                kwargs.get("nloc"),
                kwargs.get("end_line"),
                kwargs.get("start_line"),
                kwargs.get("token_count"),
                kwargs.get("top_nesting_level"),
                kwargs.get("unit_complexity_low_risk_threshold"),
                kwargs.get("unit_interfacing_low_risk_threshold"),
                kwargs.get("unit_size_low_risk_threshold"),
                json.dumps(kwargs.get("parameters")),
                kwargs.get("fan_in"),
                kwargs.get("fan_out"),
                kwargs.get("general_fan_out"),
                kwargs.get("complexity")
            )
        )
        #connection.commit()
        return cursor.lastrowid


def transform_change_type(change_type):
    if change_type == ModificationType.ADD:
        return "add"
    elif change_type == ModificationType.COPY:
        return "copy"
    elif change_type == ModificationType.RENAME:
        return "rename"
    elif change_type == ModificationType.DELETE:
        return "delete"
    elif change_type == ModificationType.MODIFY:
        return "modify"
    else:
        return "unknown"


def main():

    for repo in get_repos():
        for commit in Repository(path_to_repo=get_repos()).traverse_commits():
            print(f"inserting commit {commit.hash}")

            for modification in commit.modified_files:
                filepath = modification.filename.split(".")
                extension = ""
                if len(filepath) > 1:
                    extension = filepath[-1]



                for method in modification.methods:
                    print("method", method)

if __name__ == "__main__":
    main()
